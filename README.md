# FlashFlow-Framework

This is the header-only framework, which is the gateway for OKX. 
It takes into account some of the features of the DOCS API to reduce external delays.

## Supported API sections

* __Market data__ 
* __Orderbook trading__

## How to use

Copy the src to your project and add it to cmake

```
add_library(flashflow_framework INTERFACE)
target_include_directories(flashflow_framework INTERFACE folder_name)
target_link_libraries(dependee 
    PUBLIC/INTERFACE/PRIVATE # pick one
    flashflow_framework)
```

Requirements:

* Clang-17
* C++-23
* Boost 1.84
* Simdjson

## Additional info about OKX API

### Comparison of order routing methods using HTTP and Websocket

The standard deviation in network delays for websocket is significantly less

![comparison_network_http_ws.png](docs/network_http_vs_ws.png)

At the same time, the exchange delays in processing http requests have two modes

![comparison_server_http_ws.png](docs/server_http_vs_ws.png)

### Comparison of HTTP1/1 and HTTP/2 delays

Network delays

![http2_vs_http1.png](docs/network_http2_vs_http1.png)

Server delays

![http2_vs_http1.png](docs/server_http2_vs_http1.png)

### Using the batch API method versus without

It is better to use the appropriate API method, 
because otherwise too much latency accumulates on the server side

![batch.png](docs/batch.png)

