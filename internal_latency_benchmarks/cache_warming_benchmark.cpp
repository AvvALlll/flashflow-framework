#include <benchmark/benchmark.h>

#include <algorithm>
#include <numeric>
#include <vector>

static void BM_CacheCold(benchmark::State& state) {
  std::size_t kSize = state.range(0);
  std::vector<char> data(kSize, 1);
  std::vector<std::size_t> indices(kSize);
  std::iota(indices.begin(), indices.end(), 0);
  std::random_shuffle(indices.begin(), indices.end());

  for (auto _ : state) {
    std::size_t sum = 0;
    for (std::size_t i = 0; i < kSize; ++i) {
      benchmark::DoNotOptimize(sum += data[indices[i]]);
    }
  }
  benchmark::ClobberMemory();
}

static void BM_CacheWarm(benchmark::State& state) {
  std::size_t kSize = state.range(0);
  std::vector<char> data(kSize, 1);

  std::size_t sum_warm = 0;
  for (int i = 0; i < kSize; ++i) {
    benchmark::DoNotOptimize(sum_warm += data[i]);
  }
  benchmark::ClobberMemory();

  for (auto _ : state) {
    std::size_t sum = 0;
    for (std::size_t i = 0; i < kSize; ++i) {
      benchmark::DoNotOptimize(sum += data[i]);
    }
    benchmark::ClobberMemory();
  }
}

// The packet size is 1000, 5000, 10000. Average packet size is 5kB.
BENCHMARK(BM_CacheCold)->Arg(1000)->Arg(5000)->Arg(10000);
BENCHMARK(BM_CacheWarm)->Arg(1000)->Arg(5000)->Arg(10000);

BENCHMARK_MAIN();

// ./cache_warming_benchmark --benchmark_repetitions=10 --benchmark_report_aggregates_only=true
// g++ cache_warming_benchmark.cpp -std=c++20 -isystem benchmark/include   -Lbenchmark/build/src -lbenchmark -lpthread -o cache_warming_benchmark