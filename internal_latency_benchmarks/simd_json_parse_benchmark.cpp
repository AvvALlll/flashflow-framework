#include <benchmark/benchmark.h>
#include <simdjson.h>

#include <fstream>
#include <nlohmann/json.hpp>
#include <string>

static void BM_NaiveJsonParse(benchmark::State& state) {
  std::ifstream i("order.json");
  for (auto _ : state) {
    i.seekg(0);
    nlohmann::json j;
    i >> j;
  }
}
BENCHMARK(BM_NaiveJsonParse);

static void BM_SimdJsonParse(benchmark::State& state) {
  simdjson::dom::parser parser;
  for (auto _ : state) {
    simdjson::dom::element doc;
    auto error = parser.load("order.json").get(doc);
    if (error) {
      state.SkipWithError("JSON parsing failed");
      break;
    }
  }
}

BENCHMARK(BM_SimdJsonParse);

BENCHMARK_MAIN();