#pragma once

#include <unordered_map>
#include <string_view>

enum API {
  // Market data
  // https://www.okx.com/docs-v5/en/#order-book-trading-market-data

  Tickers = 1,
  Ticker,
  Orderbook,
  Full_orderbook,
  Candlesticks,
  Candlesticks_history,
  Trades,
  Trades_history,
  Option_trades_instid,
  Option_trades,
  Total_volume,
  Tickers_channel,
  Candlesticks_channel,
  Trades_channel,
  All_trades_channel,
  Orderbook_channel,
  Option_trades_channel,

  // Order routing and trading
  // https://www.okx.com/docs-v5/en/#order-book-trading-trade-post-place-order
  Place_order,
  Place_batch_orders,
  Cancel_order,
  Cancel_batch_orders,
  Amend_order,
  Amend_batch_orders,
  Order_channel,
  Place_order_ws,
  Place_batch_orders_ws,
  Cancel_order_ws,
  Cancel_batch_orders_ws,
  Amend_order_ws,
  Amend_batch_orders_ws,
};

std::unordered_map<API, std::string_view> api_map = {
    {API::Tickers, "tickers"},
    {API::Ticker, "ticker"},
    {API::Orderbook, "orderbook"},
    {API::Full_orderbook, "full_orderbook"},
    {API::Candlesticks, "candlesticks"},
    {API::Candlesticks_history, "candlesticks_history"},
    {API::Trades, "trades"},
    {API::Trades_history, "trades_history"},
    {API::Option_trades_instid, "option_trades_instid"},
    {API::Option_trades, "option_trades"},
    {API::Total_volume, "total_volume"},
    {API::Tickers_channel, "tickers_channel"},
    {API::Candlesticks_channel, "candlesticks_channel"},
    {API::Trades_channel, "trades_channel"},
    {API::All_trades_channel, "all_trades_channel"},
    {API::Orderbook_channel, "orderbook_channel"},
    {API::Option_trades_channel, "option_trades_channel"},


    {API::Place_order, "place_order"},
    {API::Place_batch_orders, "place_batch_orders"},
    {API::Cancel_order, "cancel_order"},
    {API::Cancel_batch_orders, "cancel_batch_orders"},
    {API::Amend_order, "amend_order"},
    {API::Amend_batch_orders, "amend_batch_orders"},
    {API::Order_channel, "order_channel"},
    {API::Place_order_ws, "place_order_ws"},
    {API::Place_batch_orders_ws, "place_batch_orders_ws"},
    {API::Cancel_order_ws, "cancel_order_ws"},
    {API::Cancel_batch_orders_ws, "cancel_batch_orders_ws"},
    {API::Amend_order_ws, "amend_order_ws"},
    {API::Amend_batch_orders_ws, "amend_batch_orders_ws"},
};


enum Op {
  Subscribe_op = 0,
  Unsubscribe_op,
  Order_op,
  Batch_orders_op,
  Cancel_order_op,
  Batch_cancel_orders_op,
  Amend_order_op,
  Batch_amend_orders_op,
  Mass_cancel_op,
};


std::unordered_map<Op, std::string_view> op_map = {
    {Op::Subscribe_op, "subscribe"},
    {Op::Unsubscribe_op, "unsubscribe"},
    {Op::Order_op, "order"},
    {Op::Batch_orders_op, "batch-orders"},
    {Op::Cancel_order_op, "cancel-order"},
    {Op::Batch_cancel_orders_op, "batch-cancel-orders"},
    {Op::Amend_order_op, "amend-order"},
    {Op::Batch_amend_orders_op, "batch-amend-orders"},
    {Op::Mass_cancel_op, "mass-cancel"},
};

std::string_view op_to_str(Op op) {
  return op_map[op];
}