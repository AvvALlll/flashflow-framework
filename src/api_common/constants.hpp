#pragma once

#include <cstdlib>

// #define DEFAULT_HTTP_SERVICE 1
// #define RECONNECT_HTTP_SERVICE 1
// #define MULTI_HTTP_SERVICE 1

#define STABLE_HTTP_SERVICE 1

// #define MULTI_WS_SERVICE 1

#ifdef DEFAULT_HTTP_SERVICE
#define stage_filename "default"
#elif RECONNECT_HTTP_SERVICE
#define stage_filename "reconnector"
#elif MULTI_HTTP_SERVICE
#define stage_filename "multi"
#elif STABLE_HTTP_SERVICE
#define stage_filename "stable"
#else
#define stage_filename "test"
#endif

#define DEMO_TRADE 1

#define metric_folder_path "/home/avval/banchmarks/batch_data/"


static const char* API_KEY = std::getenv("API_KEY");
static const char* SECRET_KEY = std::getenv("SECRET_KEY");
static const char* PASSPHRASE = std::getenv("PASSPHRASE");
