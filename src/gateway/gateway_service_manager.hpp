#pragma once

#include <memory>

#include "api_common/constants.hpp"
#include "gateway/service/rest/http_gateway_service.hpp"
#include "gateway/service/rest/http_gateway_service_base.hpp"
#include "gateway/service/rest/http_multi_gateway_service.hpp"
#include "gateway/service/rest/http_reconnect_gateway_service.hpp"
#include "gateway/service/rest/http_stable_gateway_service.hpp"
#include "gateway/service/websocket/ws_gateway_service.hpp"
#include "gateway/service/websocket/ws_gateway_service_base.hpp"
#include "gateway/service/websocket/ws_multi_gateway_service.hpp"
#include "metrics/request_metric_collector.hpp"

namespace gateway {
namespace net = boost::asio;

class gateway_service_manager {
  boost::asio::system_executor ioc_;
  net::ssl::context ctx_;

 public:
  gateway_service_manager() : ctx_(net::ssl::context::tlsv12_client) {
    ctx_.set_default_verify_paths();
    ctx_.set_verify_mode(boost::asio::ssl::verify_peer);
  }
  std::expected<std::shared_ptr<http_gateway_service_base>, utils::error>
  create_http_service() {
#ifdef RECONNECT_HTTP_SERVICE
    return create_reconnect_http_service();
#elif MULTI_HTTP_SERVICE
    return create_multi_http_service();
#elif STABLE_HTTP_SERVICE
    return create_stable_http_service();
#else
    return create_default_http_service();
#endif
  }

  std::shared_ptr<ws_gateway_service_base> create_ws_public_service() {
#ifdef MULTI_WS_SERVICE
    return create_ws_multi_service();
#else
    auto ws_service = std::make_shared<ws_gateway_service>(
        ioc_, ctx_, public_ws_resolve_info_);
    return ws_service;
#endif
  }

  std::shared_ptr<ws_gateway_service_base> create_ws_private_service() {
#ifdef MULTI_WS_SERVICE
    return std::make_shared<ws_gateway_multi_service>(ioc_, ctx_,
                                                      private_ws_resolve_info_);
#else
    return std::make_shared<ws_gateway_service>(ioc_, ctx_,
                                                private_ws_resolve_info_);
#endif
  }

  std::shared_ptr<ws_gateway_service_base> create_ws_multi_service() {
    return std::make_shared<ws_gateway_multi_service>(ioc_, ctx_,
                                                      private_ws_resolve_info_);
  }

  std::expected<std::shared_ptr<http_gateway_service_base>, utils::error>
  create_multi_http_service() {
    return std::make_shared<http_multi_gateway_service<4>>(
        ioc_, ctx_, http_resolve_info_);
  }

 private:
  std::expected<std::shared_ptr<http_gateway_service_base>, utils::error>
  create_default_http_service() {
    auto http_service =
        std::make_shared<http_gateway_service>(ioc_, ctx_, http_resolve_info_);
    return http_service;
  }

  std::expected<std::shared_ptr<http_gateway_service_base>, utils::error>
  create_reconnect_http_service() {
    auto http_service = std::make_shared<http_reconnect_gateway_service>(
        ioc_, ctx_, http_resolve_info_);
    return http_service;
  }


  std::expected<std::shared_ptr<http_gateway_service_base>, utils::error>
  create_stable_http_service() {
    auto http_service = std::make_shared<http_stable_gateway_service>(
        ioc_, ctx_, http_resolve_info_);
    return http_service;
  }

  using resolve_info_ptr = std::shared_ptr<network::resolve_info>;

  resolve_info_ptr http_resolve_info_ =
      std::make_shared<network::resolve_info>("www.okx.com", "443", "", "");

#ifndef DEMO_TRADE
  resolve_info_ptr public_ws_resolve_info_ =

      std::make_shared<network::resolve_info>("ws.okx.com", "8443",
                                              "/ws/v5/public", "");

  resolve_info_ptr private_ws_resolve_info_ =
      std::make_shared<network::resolve_info>("ws.okx.com", "8443",
                                              "/ws/v5/private", "");

  resolve_info_ptr business_ws_resolve_info_ =
      std::make_shared<network::resolve_info>("ws.okx.com", "8443",
                                              "/ws/v5/business", "");
#else
  resolve_info_ptr public_ws_resolve_info_ =

      std::make_shared<network::resolve_info>("wsaws.okx.com", "8443",
                                              "/ws/v5/public", "brokerId=9999");

  resolve_info_ptr private_ws_resolve_info_ =
      std::make_shared<network::resolve_info>(
          "wsaws.okx.com", "8443", "/ws/v5/private", "brokerId=9999");

  resolve_info_ptr business_ws_resolve_info_ =
      std::make_shared<network::resolve_info>(
          "wsaws.okx.com", "8443", "/ws/v5/business", "brokerId=9999");
#endif
};
}  // namespace gateway