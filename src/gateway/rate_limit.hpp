#pragma once

#include <cstdint>

namespace gateway {

struct rate_limit {
  int32_t request_limit;
  int32_t seconds_limit;
};
}  // namespace gateway