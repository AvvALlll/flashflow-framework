#pragma once

#include "api_common/api_methods.hpp"
#include "gateway/gateway_service_manager.hpp"
#include "gateway/request_api/base_request.hpp"
#include "gateway/service/rest/http_gateway_service_base.hpp"
#include "metrics/base_metric_collector.hpp"
#include "metrics/metric_collector_creator.hpp"

namespace request {

template <API api_request>
concept routing_http = requires { api_request == API::Place_order || api_request == API::Cancel_order; };

template <API api_request>
class http_request;

template <API api_request> requires(!routing_http<api_request>)
class http_request<api_request> : public base_request {
  std::shared_ptr<gateway::http_gateway_service_base> http_gateway_service_;
  std::shared_ptr<metrics::base_metric_collector> collector_;

 public:
  http_request(gateway::gateway_service_manager& gateway_manager,
               const metrics::metric_collector_creator& collector_creator)
      : base_request(gateway_manager),
        collector_(collector_creator.create_metric_collector(api_request)) {
    http_gateway_service_ = gateway_manager.create_http_service().value();
    http_gateway_service_->attach(collector_);
  }

  ~http_request() {
    collector_->save();
  }

  void stop() override { http_gateway_service_->stop(); }

  void execute(const network::http_request_args& args,
               std::function<void(const std::string&)> handler) override {
    http_gateway_service_->async_send_request(
        args, [handler](auto&& response) { handler(response.body()); });
  };

  inline void execute(
      network::http_request_args&& args,
      std::function<void(const std::string&)> handler) override {
    http_gateway_service_->async_send_request(
        std::move(args),
        [handler](auto&& response) { handler(response.body()); });
  };
};


template <API api_request> requires(routing_http<api_request>)
class http_request<api_request> : public base_request {
  std::shared_ptr<gateway::http_gateway_service_base> http_gateway_service_;
  std::shared_ptr<metrics::base_metric_collector> collector_;

 public:
  http_request(gateway::gateway_service_manager& gateway_manager,
               const metrics::metric_collector_creator& collector_creator)
      : base_request(gateway_manager),
        collector_(collector_creator.create_metric_collector(api_request)) {
    http_gateway_service_ = gateway_manager.create_http_service().value();
    http_gateway_service_->attach(collector_);
  }

  ~http_request() {
    collector_->save();
  }

  void stop() override { http_gateway_service_->stop(); }

  void execute(const network::http_request_args& args,
               std::function<void(const std::string&)> handler) override {
    http_gateway_service_->async_send_request(
        args, [handler](auto&& response) { handler(response.body()); });
  };

  inline void execute(
      network::http_request_args&& args,
      std::function<void(const std::string&)> handler) override {
    http_gateway_service_->async_send_request(
        std::move(args),
        [handler](auto&& response) { handler(response.body()); });
  };
};
}  // namespace request