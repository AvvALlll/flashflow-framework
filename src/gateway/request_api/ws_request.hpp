#pragma once

#include <type_traits>

#include "api_common/api_methods.hpp"
#include "gateway/request_api/base_request.hpp"
#include "message/base_message.hpp"
#include "metrics/metric_collector_creator.hpp"
#include "utils/circular_set.hpp"

namespace request {

template <API api_request>
concept routing_ws =
    api_request == API::Place_order_ws || api_request == API::Cancel_order_ws ||
    api_request == API::Amend_order_ws ||
    api_request == API::Place_batch_orders_ws ||
    api_request == API::Cancel_batch_orders_ws ||
    api_request == API::Amend_batch_orders_ws ||
    api_request == API::Order_channel;

template <API api_request>
class ws_request;

template <API api_request>
requires(!routing_ws<api_request>) class ws_request<api_request>
    : public base_request {
  using base_request::base_request;
  std::shared_ptr<gateway::ws_gateway_service_base> ws_gateway_service_;
  std::shared_ptr<metrics::base_metric_collector> collector_;
  utils::circular_set<4096> history_set_;

 public:
  ws_request(gateway::gateway_service_manager& gateway_manager,
             gateway::listener_t listener,
             const metrics::metric_collector_creator& collector_creator)
      : base_request(gateway_manager),
        collector_(
            collector_creator.create_channel_metric_collector(api_request)) {
    ws_gateway_service_ = gateway_manager.create_ws_public_service();
    ws_gateway_service_->attach(collector_);
    ws_gateway_service_->attach_listener([&collector = collector_,
                                          &history_set = history_set_,
                                          listener](const std::string& data) {
      if (history_set.push(data)) {
        collector->collect_metric(data);
        listener(data);
      }
    });
  }

  ~ws_request() { collector_->save(); }

  void stop() override { ws_gateway_service_->stop(); }

  void ws_execute(const message::ws_request_message& ws_request) override {
    ws_gateway_service_->async_send(ws_request);
  }

  void ws_execute(message::ws_request_message&& ws_request) override {
    ws_gateway_service_->async_send(std::move(ws_request));
  }
};

template <API api_request>
requires(routing_ws<api_request>) class ws_request<api_request>
    : public base_request {
  using base_request::base_request;
  std::shared_ptr<gateway::ws_gateway_service_base> ws_gateway_service_;
  std::shared_ptr<metrics::base_metric_collector> collector_;

  utils::circular_set<1024> history_set_;

 public:
  ws_request(gateway::gateway_service_manager& gateway_manager,
             gateway::listener_t listener,
             const metrics::metric_collector_creator& collector_creator)
      : base_request(gateway_manager),
        collector_(collector_creator.create_ws_metric_collector(api_request)) {
    ws_gateway_service_ = gateway_manager.create_ws_private_service();
    ws_gateway_service_->attach(collector_);
    ws_gateway_service_->attach_listener(
        [listener](const std::string& data) { listener(data); });
    ws_gateway_service_->attach_prestart();
    ws_gateway_service_->login();
  }

  ~ws_request() { collector_->save(); }

  void stop() override { ws_gateway_service_->stop(); }

  void ws_execute(const message::ws_request_message& ws_request) override {
    ws_gateway_service_->async_send(ws_request);
  }

  void ws_execute(message::ws_request_message&& ws_request) override {
    ws_gateway_service_->async_send(std::move(ws_request));
  }
};

template <>
class ws_request<API::Order_channel> : public base_request {
  using base_request::base_request;
  std::shared_ptr<gateway::ws_gateway_service_base> ws_gateway_service_;
  std::shared_ptr<metrics::base_metric_collector> collector_;
  utils::circular_set<2048> history_set_;

 public:
  ws_request(gateway::gateway_service_manager& gateway_manager,
             gateway::listener_t listener,
             const metrics::metric_collector_creator& collector_creator)
      : base_request(gateway_manager),
        collector_(collector_creator.create_channel_metric_collector(
            API::Order_channel)) {
    ws_gateway_service_ = gateway_manager.create_ws_private_service();
    ws_gateway_service_->attach_listener([&collector = collector_,
                                          &history_set = history_set_,
                                          listener](const std::string& data) {
      if (history_set.push(data)) {
        collector->collect_metric(data);
        listener(data);
      }
    });
    ws_gateway_service_->attach_prestart();
    ws_gateway_service_->login();
  }

  ~ws_request() { collector_->save(); }

  void stop() override { ws_gateway_service_->stop(); }

  void ws_execute(const message::ws_request_message& request) override {
    ws_gateway_service_->async_send(request);
  }

  void ws_execute(message::ws_request_message&& request) override {
    ws_gateway_service_->async_send(std::move(request));
  }
};
}  // namespace request