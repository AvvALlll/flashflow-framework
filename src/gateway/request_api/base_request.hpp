#pragma once

#include "gateway/gateway_service_manager.hpp"
#include "gateway/rate_limit.hpp"
#include "message/base_message.hpp"

namespace request {

struct client_info {
  inline static const char* API_KEY = std::getenv("API_KEY");;
  inline static const char* SECRET_KEY = std::getenv("SECRET_KEY");;
  inline static const char* PASSPHRASE = std::getenv("PASSPHRASE");
};

class base_request {
  gateway::gateway_service_manager& gateway_manager_;

 public:
  base_request(gateway::gateway_service_manager& gateway_manager)
      : gateway_manager_(gateway_manager) {}
  virtual ~base_request() {}
  virtual void ws_execute(const message::ws_request_message&) {}
  virtual void ws_execute(message::ws_request_message&&) {}
  
  virtual void execute(const network::http_request_args&) {}
  virtual void execute(network::http_request_args&&) {}
  virtual void execute(const network::http_request_args&, std::function<void(const std::string&)>) {}
  virtual void execute(network::http_request_args&&, std::function<void(const std::string&)>) {}

  virtual void stop() = 0;
};
}  // namespace request