#pragma once

#include <memory>
#include <vector>

#include "gateway/service/websocket/ws_gateway_service_base.hpp"
#include "message/base_message.hpp"
#include "network/websocket/websocket_channel.hpp"
#include "utils/algo.hpp"

namespace gateway {
namespace net = boost::asio;

class ws_gateway_multi_service : public ws_gateway_service_base {
  using ws_channel_ptr = std::shared_ptr<network::websocket_channel>;
  std::vector<ws_channel_ptr> channels;
  std::shared_ptr<network::resolve_info> resolve_info_;

 public:
  ws_gateway_multi_service(
      net::any_io_executor ex, net::ssl::context& ctx,
      std::shared_ptr<network::resolve_info> resolve_info) {
    for (size_t i = 0; i < 10; ++i) {
      channels.emplace_back(
          std::make_shared<network::websocket_channel>(ex, ctx, *resolve_info));
    }
  }

  void login() override {
    for (auto& channel : channels) {
      channel->async_send(create_login_message());
    }
  }
  void async_send(message::ws_request_message&& ws_request) override {
    for (auto& channel : channels) {
      collector_->register_metric(channel->channel_id());
      channel->async_send(ws_request);
    }
  }

  void async_send(const message::ws_request_message& ws_request) override {
    for (auto& channel : channels) {
      collector_->register_metric(channel->channel_id());
      channel->async_send(ws_request);
    }
  }

  void stop() override {
    for (auto& channel : channels) {
      channel->async_close();
    }
  }

  void attach_listener(const listener_t& listener) override {
    for (auto& channel : channels) {
      channel->set_listener([&channel, listener, &collector = collector_](
                                const std::string& response) {
        if (collector)
          collector->collect_metric(channel->channel_id(), response);
        else {
          printf("no collector\n");
        }
        listener(response);
      });
    }
  }

  void attach_prestart() override {
    for (auto& channel : channels)
      channel->set_prestart([this, &channel] {
        if (collector_) collector_->roll_back(channel->channel_id());
        channel->async_send(create_login_message());
      });
  }
};
}  // namespace gateway