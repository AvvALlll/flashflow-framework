#pragma once
#include <list>
#include <memory>
#include <optional>

#include "api_common/constants.hpp"
#include "message/base_message.hpp"
#include "metrics/request_metric_collector.hpp"
#include "network/resolver.hpp"
#include "utils/algo.hpp"

namespace gateway {
using listener_t = std::function<void(const std::string&)>;
using prestart_t = std::function<void()>;

class ws_gateway_service_base {
 protected:
  std::shared_ptr<metrics::base_metric_collector> collector_;
  listener_t listener_;

 public:
  using listener_t = std::function<void(std::string)>;
  virtual ~ws_gateway_service_base(){};

  virtual void login() = 0;

  virtual void stop() = 0;

  virtual void async_send(message::ws_request_message&&) = 0;
  virtual void async_send(const message::ws_request_message&) = 0;

  virtual void attach_prestart() = 0;

  virtual void attach_listener(const listener_t& listener) {
    listener_ = listener;
  }

  virtual void detach_listener() { listener_ = {}; }

  virtual void attach(
      std::shared_ptr<metrics::base_metric_collector> collector) {
    collector_ = std::move(collector);
  }

  virtual void detach(
      std::shared_ptr<metrics::base_metric_collector> collector) {
    if (collector_ && collector_ == collector) {
      collector_.reset();
    }
  }

 protected:
  message::ws_request_message create_login_message() {
    std::string timestamp = std::to_string(utils::timestamp());
    return message::ws_request_message(
        "login", message::request_args::dyn_args_format(
                     {{"apiKey", API_KEY},
                      {"passphrase", PASSPHRASE},
                      {"timestamp", timestamp},
                      {"sign", utils::sing(SECRET_KEY, timestamp)}}));
  }
};
}  // namespace gateway