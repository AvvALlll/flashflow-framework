#pragma once

#include "gateway/service/websocket/ws_gateway_service_base.hpp"
#include "message/base_message.hpp"
#include "network/websocket/websocket_channel.hpp"
#include "utils/algo.hpp"
#include "utils/utils_function.hpp"

namespace gateway {
namespace net = boost::asio;
class ws_gateway_service : public ws_gateway_service_base {
  network::websocket_channel ws;

 public:
  ws_gateway_service(net::any_io_executor ex, net::ssl::context& ctx,
                     std::shared_ptr<network::resolve_info> info)
      : ws(ex, ctx, *info){};

  void login() override { ws.async_send(create_login_message()); }

  void stop() override { ws.async_close(); }

  void async_send(message::ws_request_message&& ws_request) override {
    if (collector_) collector_->register_metric();
    ws.async_send(std::move(ws_request));
  }

  void async_send(const message::ws_request_message& ws_request) override {
    if (collector_) collector_->register_metric();
    ws.async_send(ws_request);
  }

  void attach_listener(const listener_t& listener) override {
    ws.set_listener(
        [&collector = collector_, listener](const std::string& response) {
          if (collector) collector->collect_metric(response);
          listener(response);
        });
  }

  void attach_prestart() override {
    ws.set_prestart([this] {
      if (collector_) {
        collector_->roll_back();
      }
      login();
    });
  }
};
}  // namespace gateway