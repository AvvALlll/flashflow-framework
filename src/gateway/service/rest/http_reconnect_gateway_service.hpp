
#include "boost/asio/any_io_executor.hpp"
#include "gateway/service/rest/http_gateway_service_base.hpp"

namespace gateway {
class http_reconnect_gateway_service : public http_gateway_service_base {
  using http_service_ptr = std::shared_ptr<network::http_service>;
  http_service_ptr http_service_;
  std::shared_ptr<network::resolve_info> resolve_info_;

 public:
  http_reconnect_gateway_service(
      net::any_io_executor ex, net::ssl::context& ctx,
      std::shared_ptr<network::resolve_info> resolve_info)
      : http_service_(std::make_shared<network::http_service>(ex, ctx)),
        resolve_info_(std::move(resolve_info)) {}

  utils::ok_result<void> connect() override {
    return http_service_->start(resolve_info_);
  }

  void stop() override { http_service_->close(); }

  void send_request(
      const network::http_request_args& args,
      network::read_http_completion_handler_t response_handler) override {
    if (collector_) collector_->register_metric();
    if (!http_service_->is_open()) connect();
    http_service_->send_request(args);
    auto response = http_service_->read_response();
    if (collector_) collector_->collect_metric(response.body());
    response_handler(std::move(response));
    http_service_->close();
  }

  void async_send_request(
      const network::http_request_args& args,
      network::read_http_completion_handler_t response_handler) override {
    auto write_handler = [this, response_handler,
                          &args](http_service_ptr http_service) {
      http_service->async_send_request(
          args,
          [this, response_handler](auto http_service) {
            http_service->async_read_response(
                [this, response_handler,
                 http_service = std::move(http_service)](auto&& response) {
                  if (collector_) collector_->collect_metric(response.body());
                  response_handler(std::move(response));
                  http_service->close();
                },
                error_handler_);
          },
          error_handler_);
    };

    if (collector_) collector_->register_metric();

    if (!http_service_->is_open()) {
      LOG_TRACE("http_reconnecter connect");
      http_service_->async_start(resolve_info_, write_handler);
      return;
    }

    write_handler(http_service_);
  }

  void async_send_request(
      network::http_request_args&& args,
      network::read_http_completion_handler_t response_handler) override {
    auto write_handler = [this, response_handler, args = std::move(args)](
                             http_service_ptr http_service) {
      http_service->async_send_request(
          std::move(args),
          [this, response_handler](auto http_service) {
            http_service->async_read_response(
                [this, response_handler,
                 http_service = std::move(http_service)](auto&& response) {
                  if (collector_) collector_->collect_metric(response.body());
                  response_handler(std::move(response));
                  http_service->close();
                },
                error_handler_);
          },
          error_handler_);
    };

    if (collector_) collector_->register_metric();

    if (!http_service_->is_open()) {
      LOG_TRACE("[rv] http_reconnecter connect");
      http_service_->async_start(resolve_info_, write_handler);
      return;
    }

    write_handler(http_service_);
  }
};
}  // namespace gateway