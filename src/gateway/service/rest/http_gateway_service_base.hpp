#pragma once

#include <functional>
#include <memory>
#include <optional>

#include "metrics/request_metric_collector.hpp"
#include "network/http/http_service.hpp"

namespace gateway {

namespace net = boost::asio;

class http_gateway_service_base {
 protected:
  std::shared_ptr<metrics::base_metric_collector> collector_;
  network::read_http_completion_handler_t response_handler_;
  network::error_handler_t error_handler_ =
      [&collector = collector_](boost::beast::error_code) {
        if (collector) collector->roll_back();
      };

 public:
  virtual ~http_gateway_service_base() {}

  virtual utils::ok_result<void> connect() = 0;

  virtual void send_request(const network::http_request_args&,
                            network::read_http_completion_handler_t) = 0;

  virtual void async_send_request(const network::http_request_args&,
                                  network::read_http_completion_handler_t) = 0;
  virtual void async_send_request(network::http_request_args&&,
                                  network::read_http_completion_handler_t) = 0;

  virtual void async_send_request(const network::http_request_args&){};
  virtual void async_send_request(network::http_request_args&&){};

  virtual void stop() = 0;

  virtual void attcach_response_handler(
      const network::read_http_completion_handler_t& response_handler) {
    response_handler_ = response_handler;
  }
  virtual void detach_response_handler() { response_handler_ = {}; }

  virtual void attach(
      std::shared_ptr<metrics::base_metric_collector> collector) {
    collector_ = std::move(collector);
  }

  virtual void detach(
      std::shared_ptr<metrics::base_metric_collector> collector) {
    if (collector_ && collector_ == collector) {
      collector_.reset();
    }
  }
};
}  // namespace gateway