#pragma once

#include <iostream>
#include <thread>

#include "boost/asio/any_io_executor.hpp"
#include "gateway/service/rest/http_gateway_service_base.hpp"

namespace gateway {

class http_gateway_service : public http_gateway_service_base {
  using http_service_ptr = std::shared_ptr<network::http_service>;
  http_service_ptr http_service_;
  std::shared_ptr<network::resolve_info> resolve_info_;

 public:
  http_gateway_service(net::any_io_executor ex, net::ssl::context& ctx,
                       std::shared_ptr<network::resolve_info> resolve_info)
      : http_service_(std::make_shared<network::http_service>(ex, ctx)),
        resolve_info_(std::move(resolve_info)) {
    connect();
  }

  utils::ok_result<void> connect() override {
    return http_service_->start(resolve_info_);
  }
  void stop() override { http_service_->close(); }

  void send_request(
      const network::http_request_args& args,
      network::read_http_completion_handler_t response_handler) override {
    if (collector_) collector_->register_metric();
    if (!http_service_->is_open()) connect();
    http_service_->send_request(args);
    auto response = http_service_->read_response();
    if (collector_) collector_->collect_metric(response.body());
    response_handler(std::move(response));
  }

  void async_send_request(const network::http_request_args& args) override {
    auto write_handler = [this, &args](http_service_ptr http_service) {
      http_service->async_send_request(
          args,
          [this](auto http_service) {
            http_service->async_read_response(
                [this](auto&& response) {
                  if (collector_) collector_->collect_metric(response.body());
                  if (response_handler_) response_handler_(std::move(response));
                },
                error_handler_);
          },
          error_handler_);
    };

    if (collector_) collector_->register_metric();

    if (!http_service_->is_open()) {
      LOG_INFO("http connect");
      http_service_->async_start(resolve_info_, write_handler);
      return;
    }

    write_handler(http_service_);
  }

  void async_send_request(network::http_request_args&& args) override {
    auto write_handler =
        [this, args = std::move(args)](http_service_ptr http_service) {
          http_service->async_send_request(
              std::move(args),
              [this](auto http_service) {
                http_service->async_read_response(
                    [this](auto&& response) {
                      if (collector_)
                        collector_->collect_metric(response.body());
                      if (response_handler_)
                        response_handler_(std::move(response));
                    },
                    error_handler_);
              },
              error_handler_);
        };

    if (collector_) collector_->register_metric();
    if (!http_service_->is_open()) {
      LOG_INFO("[rv] http connect");
      http_service_->async_start(resolve_info_, write_handler);
      return;
    }

    write_handler(http_service_);
  }

  void async_send_request(
      const network::http_request_args& args,
      network::read_http_completion_handler_t response_handler) override {
    auto write_handler = [this, &args,
                          response_handler](http_service_ptr http_service) {
      http_service->async_send_request(
          args,
          [this, response_handler](auto http_service) {
            http_service->async_read_response(
                [this, response_handler](auto&& response) {
                  if (collector_) collector_->collect_metric(response.body());
                  response_handler(std::move(response));
                },
                error_handler_);
          },
          error_handler_);
    };

    if (collector_) collector_->register_metric();

    if (!http_service_->is_open()) {
      LOG_INFO("http connect");
      http_service_->async_start(resolve_info_, write_handler);
      return;
    }

    write_handler(http_service_);
  }

  void async_send_request(
      network::http_request_args&& args,
      network::read_http_completion_handler_t response_handler) override {
    auto write_handler = [this, response_handler, args = std::move(args)](
                             http_service_ptr http_service) {
      http_service->async_send_request(
          std::move(args),
          [this, response_handler](auto http_service) {
            http_service->async_read_response(
                [this, response_handler](auto&& response) {
                  if (collector_) collector_->collect_metric(response.body());
                  response_handler(std::move(response));
                },
                error_handler_);
          },
          error_handler_);
    };

    if (collector_) collector_->register_metric();
    if (!http_service_->is_open()) {
      LOG_INFO("[rv] http connect");
      http_service_->async_start(resolve_info_, write_handler);
      return;
    }

    write_handler(http_service_);
  }
};
}  // namespace gateway