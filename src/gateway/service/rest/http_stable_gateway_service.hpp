#pragma once
#include <__atomic/aliases.h>

#include "gateway/service/rest/http_gateway_service_base.hpp"
#include "utils/utils_function.hpp"

namespace gateway {

class http_stable_gateway_service : public http_gateway_service_base {
  using http_service_ptr = std::shared_ptr<network::http_service>;
  std::vector<http_service_ptr> http_services_;
  std::shared_ptr<network::resolve_info> resolve_info_;
  std::mutex timer_mt_;
  boost::asio::steady_timer timer_;
  std::atomic_size_t cur_service_ = 0;

 public:
  http_stable_gateway_service(
      net::any_io_executor ex, net::ssl::context& ctx,
      std::shared_ptr<network::resolve_info> resolve_info)
      : resolve_info_(std::move(resolve_info)),
        timer_(ex, boost::asio::chrono::seconds(9)) {
    for (size_t i = 0; i < 2; ++i)
      http_services_.emplace_back(
          std::make_shared<network::http_service>(ex, ctx));
    http_services_[0]->start(resolve_info_);
    timer_.expires_from_now(boost::asio::chrono::seconds(9));
    timer_.async_wait(
        std::bind(&http_stable_gateway_service::connection_stabilization, this,
                  boost::asio::placeholders::error));
  }
  utils::ok_result<void> connect() override {
    for (auto& service : http_services_) {
      service->start(resolve_info_);
    }
    return {};
  }

  void stop() override {
    std::lock_guard lock(timer_mt_);
    timer_.cancel();
    for (auto& service : http_services_) {
      service->close();
    }
  }

  void send_request(
      const network::http_request_args& args,
      network::read_http_completion_handler_t response_handler) override {
    restart_timer();
    auto http_service = http_services_[cur_service_];
    if (collector_) collector_->register_metric();
    if (!http_service->is_open()) http_service->start(resolve_info_);
    http_service->send_request(args);
    auto response = http_service->read_response();
    if (collector_) collector_->collect_metric(response.body());
    response_handler(std::move(response));
  };

  void async_send_request(
      const network::http_request_args& args,
      network::read_http_completion_handler_t response_handler) override {
    restart_timer();
    auto http_service = http_services_[cur_service_];
    auto write_handler = [this, response_handler,
                          args](http_service_ptr http_service) {
      http_service->async_send_request(
          args,
          [this, response_handler](auto http_service) {
            http_service->async_read_response(
                [this, response_handler,
                 http_service = http_service](auto&& response) {
                  if (collector_) collector_->collect_metric(response.body());
                  response_handler(std::move(response));
                },
                error_handler_);
          },
          error_handler_);
    };
    if (collector_) collector_->register_metric();

    if (!http_service->is_open()) {
      LOG_TRACE("http_reconnecter connect");
      http_service->async_start(resolve_info_, write_handler);
      return;
    }

    write_handler(http_service);
  }
  void async_send_request(
      network::http_request_args&& args,
      network::read_http_completion_handler_t response_handler) override {
    restart_timer();
    LOG_TRACE("cur service: ", cur_service_);
    auto http_service = http_services_[cur_service_];
    auto write_handler = [this, response_handler, args = std::move(args)](
                             http_service_ptr http_service) {
      http_service->async_send_request(
          std::move(args),
          [this, response_handler](auto http_service) {
            http_service->async_read_response(
                [this, response_handler,
                 http_service = std::move(http_service)](auto&& response) {
                  if (collector_) collector_->collect_metric(response.body());
                  response_handler(std::move(response));
                },
                error_handler_);
          },
          error_handler_);
    };

    if (collector_) collector_->register_metric();

    if (!http_service->is_open()) {
      LOG_TRACE("http_reconnecter connect");
      http_service->async_start(resolve_info_, write_handler);
      return;
    }

    write_handler(http_service);
  }

 private:
  void restart_timer() {
    std::lock_guard lock(timer_mt_);
    timer_.cancel();
    timer_.expires_from_now(boost::asio::chrono::seconds(9));
    timer_.async_wait(
        std::bind(&http_stable_gateway_service::connection_stabilization, this,
                  boost::asio::placeholders::error));
  }

  void connection_stabilization(const boost::system::error_code& err) {
    LOG_TRACE("[conn stabilization]: timer operation");
    if (err) {
      LOG_TRACE("[timer]: ", err.message());
      return;
    }
    size_t cur_conn = cur_service_;
    size_t next_conn = (cur_conn + 1) % http_services_.size();
    http_services_[next_conn]->start(resolve_info_);
    cur_service_ = next_conn;
    http_services_[cur_conn]->close();

    std::lock_guard lock(timer_mt_);
    timer_.expires_from_now(boost::asio::chrono::seconds(9));
    timer_.async_wait(
        std::bind(&http_stable_gateway_service::connection_stabilization, this,
                  boost::asio::placeholders::error));
  }
};
}  // namespace gateway