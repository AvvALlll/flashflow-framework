#pragma once

#include "gateway/service/rest/http_gateway_service_base.hpp"
#include "utils/circular_set.hpp"
#include "utils/utils_function.hpp"

namespace gateway {
template <size_t SockCount = 4>
class http_multi_gateway_service : public http_gateway_service_base {
  using http_service_ptr = std::shared_ptr<network::http_service>;
  std::vector<http_service_ptr> http_services_;
  std::shared_ptr<network::resolve_info> resolve_info_;
  utils::random_generator generator;

  utils::circular_set<4096> response_set_;

 public:
  http_multi_gateway_service(
      net::any_io_executor ex, net::ssl::context& ctx,
      std::shared_ptr<network::resolve_info> resolve_info)
      : resolve_info_(std::move(resolve_info)), generator(0, SockCount - 1) {
    for (size_t i = 0; i < SockCount; ++i)
      http_services_.emplace_back(
          std::make_shared<network::http_service>(ex, ctx));
    connect();
  }
  utils::ok_result<void> connect() override {
    for (auto& service : http_services_) {
      service->start(resolve_info_);
    }
    return {};
  }

  void stop() override {
    for (auto& service : http_services_) {
      service->close();
    }
  }

  void send_request(
      const network::http_request_args& args,
      network::read_http_completion_handler_t response_handler) override {
    if (collector_) collector_->register_metric();
    for (auto& service : http_services_) {
      if (!service->is_open()) service->start(resolve_info_);
      service->send_request(args);
      auto response = service->read_response();
      response_handler(std::move(response));
      if (collector_) collector_->collect_metric(response.body());
    }
  };

  void async_send_request(
      const network::http_request_args& args,
      network::read_http_completion_handler_t response_handler) override {
    auto write_handler = [this, response_handler,
                          args](http_service_ptr http_service) {
      http_service->async_send_request(
          args,
          [this, response_handler](auto http_service) {
            http_service->async_read_response(
                [this, response_handler, http_service](auto&& response) {
                  auto response_str = response.body();
                  if (collector_) collector_->collect_metric(response_str);
                  response_handler(std::move(response));
                },
                error_handler_);
          },
          error_handler_);
    };
    if (collector_) collector_->register_metric();

    for (auto& http_service : http_services_) {
      if (!http_service->is_open()) {
        LOG_ERROR("http_reconnecter connect");
        http_service->async_start(resolve_info_, write_handler);
        return;
      }

      write_handler(http_service);
    }
  }
  void async_send_request(
      network::http_request_args&& args,
      network::read_http_completion_handler_t response_handler) override {
    auto write_handler = [this, response_handler, args = std::move(args)](
                             http_service_ptr http_service) {
      http_service->async_send_request(
          std::move(args),
          [this, response_handler](auto http_service) {
            http_service->async_read_response(
                [this, response_handler,
                 http_service = std::move(http_service)](auto&& response) {
                  // auto response_str = response.body();
                  if (collector_) collector_->collect_metric(response.body());
                  response_handler(std::move(response));
                },
                error_handler_);
          },
          error_handler_);
    };

    if (collector_) collector_->register_metric();

    for (auto& http_service : http_services_) {
      if (!http_service->is_open()) {
        LOG_TRACE("http_reconnecter connect");
        http_service->async_start(resolve_info_, write_handler);
        return;
      }

      write_handler(http_service);
    }
  }
};
}  // namespace gateway