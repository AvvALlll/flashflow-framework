#pragma once

#include <cstdint>
#include <fstream>
#include <string>

#include "metrics/base_metric_collector.hpp"
#include "utils/error.hpp"
#include "utils/logging.hpp"

namespace metrics {
struct ws_stat_info {
  uint64_t received_time;
  std::string data;

  friend std::fstream& operator<<(std::fstream& file,
                                  const ws_stat_info& stat) {
    file << "{"
         << "\"received_time\":" << stat.received_time
         << ", \"data\":" << stat.data << "}";
    return file;
  }
};

class channel_metric_collector : public base_metric_collector {
  size_t part_id_ = 0;
  std::mutex metric_collection_mt_;
  std::vector<ws_stat_info> metric_collection_;

  std::string folder_path_;
  std::string first_name_;
  std::string filename_;

 public:
  channel_metric_collector(const std::string& folder_path,
                           const std::string& first_name,
                           const std::string& filename)
      : folder_path_(folder_path),
        first_name_(first_name),
        filename_(filename) {
    std::filesystem::remove_all(folder_path_);
    std::filesystem::create_directory(folder_path_);
  }

  void collect_metric(const std::string& data) override {
    auto received_time =
        std::chrono::system_clock::now().time_since_epoch().count();
    std::lock_guard lock(metric_collection_mt_);
    metric_collection_.emplace_back(received_time, data);
    if (metric_collection_.size() >= 10000 && save()) {
      metric_collection_.clear();
    }
  }

  utils::ok_result<void> save() override {
    if (metric_collection_.empty()) return {};

    std::fstream file(folder_path_ + std::to_string(part_id_++) + "_" +
                          first_name_ + "_" + filename_,
                      std::fstream::out);
    if (!file.is_open()) {
      LOG_ERROR("cannot open a file for save");
      return std::unexpected(utils::error{"cannot open file"});
    }
    file << "{\n"
         << R"("measures":[)";
    write_metric_collection(file) << "]\n}\n";

    return {};
  }

 private:
  std::fstream& write_metric_collection(std::fstream& file) {
    for (size_t i = 0; i < metric_collection_.size(); ++i) {
      file << metric_collection_[i];
      if (i != metric_collection_.size() - 1) file << ",";
    }
    return file;
  }
};
}  // namespace metrics