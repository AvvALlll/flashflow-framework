#pragma once
#include <string>
#include "utils/error.hpp"

namespace metrics {

class base_metric_collector {
 public:
  virtual ~base_metric_collector() = default;
  virtual void register_metric(){};
  virtual void register_metric(size_t) {};
  virtual void collect_metric(const std::string&) {};
  virtual void collect_metric(size_t, const std::string&) {};
  virtual void roll_back() {};
  virtual void roll_back(size_t) {};
  virtual utils::ok_result<void> save() = 0;
};
}  // namespace metrics