#pragma once

#include "api_common/api_methods.hpp"
#include "metrics/channel_metric_collector.hpp"
#include "metrics/request_metric_collector.hpp"
#include "metrics/request_ws_metric_collector.hpp"

namespace metrics {
class metric_collector_creator {
  std::string folder_path_;
  std::string first_name_;

 public:
  metric_collector_creator(const std::string& folder_path,
                           const std::string& first_name)
      : folder_path_(folder_path), first_name_(first_name) {}
  std::shared_ptr<base_metric_collector> create_metric_collector(
      API api_method) const {
    std::string str_api_method = std::string(api_map[api_method]);
    return std::make_shared<request_metric_collector>(
        folder_path_ + str_api_method + "/", first_name_,
        str_api_method + ".json");
  }

  std::shared_ptr<base_metric_collector> create_channel_metric_collector(
      API api_method) const {
    std::string str_api_method = std::string(api_map[api_method]);
    return std::make_shared<channel_metric_collector>(
        folder_path_ + str_api_method + "/", first_name_,
        str_api_method + ".json");
  }

  std::shared_ptr<base_metric_collector> create_ws_metric_collector(
      API api_method) const {
    std::string str_api_method = std::string(api_map[api_method]);
    return std::make_shared<request_ws_metric_collector>(
        folder_path_ + str_api_method + "/", first_name_,
        str_api_method + ".json");
  }
};
}  // namespace metrics