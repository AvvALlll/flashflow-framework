#pragma once

#include "metrics/collecting_system/base_collecting_system.hpp"
#include "metrics/request_metric_collector.hpp"
#include "network/http/http_service.hpp"
#include "api_common/constants.hpp"
#include "metrics/metric_collector_creator.hpp"

namespace collecting_system {
namespace net = boost::asio;

class pure_collecting_system : public base_collecting_system {
  net::strand<boost::asio::system_executor> ioc_;
  net::ssl::context ctx_;
  metrics::metric_collector_creator collector_creator;

 public:
  pure_collecting_system() : ctx_(net::ssl::context::tlsv12_client), collector_creator(metric_folder_path, stage_filename) {
    ctx_.set_default_verify_paths();
    ctx_.set_verify_mode(boost::asio::ssl::verify_peer);
  }
  void run() override {
    run_rest_api();
  }

 private:
  void run_rest_api() {
    auto http = std::make_shared<network::http_service>(ioc_, ctx_);
    http->start(http_resolve_info_);
    auto request_args = network::http_request_args{boost::beast::http::verb::head,
                                ""};

    auto collector = collector_creator.create_metric_collector(API::Ticker);
    // for (size_t i = 0; i < 1000; ++i) {
    collector->register_metric();
    http->async_send_request({boost::beast::http::verb::get,
                              "/api/v5/market/ticker?instId=BTC-USD-SWAP"},
                              [&collector = collector](auto http_service) {
                                http_service->async_read_response(
                                    [&collector = collector](auto&& response) {
                                      collector->collect_metric(response.body());
                                      std::cout << response.body() << std::endl;
                                    }, [](auto) {});
                              }, [](auto) {});
    sleep(16);
    http->async_send_request({boost::beast::http::verb::head,
                              ""},
                              [&collector = collector](auto http_service) {
                                http_service->async_read_response(
                                    [&collector = collector](auto&& response) {
                                      collector->collect_metric(response.body());
                                      std::cout << response.body() << std::endl;
                                    }, [](auto) {});
                              }, [](auto) {});
    // sleep(8);
    //  http->async_send_request({boost::beast::http::verb::get,
    //                           "/api/v5/market/ticker?instId=BTC-USD-SWAP"},
    //                           [&collector = collector](auto http_service) {
    //                             http_service->async_read_response(
    //                                 [&collector = collector](auto&& response) {
    //                                   collector.collect_metric(response.body());
    //                                   std::cout << response.body() << std::endl;
    //                                 });
    //                           });
    //   usleep(100000);
    // }
    
    collector->save();

     boost::asio::query(boost::asio::system_executor(),
                       boost::asio::execution::context)
        .join();
  }

  std::shared_ptr<network::resolve_info> http_resolve_info_ =
      std::make_shared<network::resolve_info>("www.okx.com", "443", "", "");

  std::shared_ptr<network::resolve_info> public_ws_resolve_info_ =

      std::make_shared<network::resolve_info>("ws.okx.com", "8443",
                                              "/ws/v5/public", "");
};
}  // namespace collecting_system