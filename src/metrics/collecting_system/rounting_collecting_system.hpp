#pragma once

#include <thread>

#include "api/order-routing/order_routing_api.hpp"
#include "api/order-routing/order_routing_test_handler.hpp"
#include "metrics/collecting_system/base_collecting_system.hpp"

namespace collecting_system {
class routing_collecting_system : public base_collecting_system {
  order_routing::order_routing_api or_api;

 public:
  routing_collecting_system()
      : or_api(std::make_unique<order_routing::order_routing_handler_test>()) {
    sleep(1);
    or_api.subscribe_order();
  }
  virtual void run() override {
    run_routing();
    // run_batch_ordering();
    // run_place_consecutive_orders();
    or_api.stop();
    boost::asio::query(boost::asio::system_executor(),
                       boost::asio::execution::context)
        .join();
  }

 private:
  void run_routing() {
    for (size_t i = 0; i < 1000; ++i) {
          message::request_args ws_args({{"instId", "BTC-USDT"},
                                   {"tdMode", "cash"},
                                   {"side", "buy"},
                                   {"ordType", "limit"},
                                   {"px", "2.15"},
                                   {"sz", "2"},
                                   {"clOrdId", "placews" + std::to_string(i)}});


    message::request_args ws_cancel_args(
        {{"instId", "BTC-USDT"}, {"clOrdId", "placews" + std::to_string(i)}});

      or_api.place_order_ws(ws_args);
      // or_api.place_order(http_args);
      usleep(500000);
      or_api.cancel_order_ws(ws_cancel_args);
      // or_api.cancel_order(http_cancel_args);
      usleep(100000);
    }
  }

  void run_place_consecutive_orders() {
    for (size_t i = 0; i < 1000; ++i) {
      message::request_args http_args(
          {{"instId", "BTC-USDT"},
           {"tdMode", "cash"},
           {"side", "buy"},
           {"ordType", "limit"},
           {"px", "2.15"},
           {"sz", "2"},
           {"clOrdId", "placehttp" + std::to_string(i)}});
      or_api.place_order(http_args);
      usleep(100000);
    }

    for (size_t i = 0; i < 1000; ++i) {
      message::request_args http_cancel_args(
          {{"instId", "BTC-USDT"},
           {"clOrdId", "placehttp" + std::to_string(i)}});
      or_api.cancel_order(http_cancel_args);
      usleep(100000);
    }

    for (size_t i = 0; i < 1000; ++i) {
      message::request_args ws_args(
          {{"instId", "BTC-USDT"},
           {"tdMode", "cash"},
           {"side", "buy"},
           {"ordType", "limit"},
           {"px", "2.15"},
           {"sz", "2"},
           {"clOrdId", "placews" + std::to_string(i)}});
      or_api.place_order_ws(ws_args);
      usleep(100000);
    }

    for (size_t i = 0; i < 1000; ++i) {
      message::request_args ws_cancel_args(
          {{"instId", "BTC-USDT"}, {"clOrdId", "placews" + std::to_string(i)}});
      or_api.cancel_order_ws(ws_cancel_args);
      usleep(100000);
    }
  }

  void run_batch_ordering() {
    const size_t batch_size = 1;
    const size_t number_of_experiments = 500;

    sleep(1);
    for (size_t k = 0; k < number_of_experiments; ++k) {
      std::vector<message::request_args> place_args_array_single;
      std::vector<message::request_args> cancel_args_array_single;
      std::vector<message::request_args> place_args_array_batch;
      std::vector<message::request_args> cancel_args_array_batch;
      for (size_t i = 0; i < batch_size; ++i) {
        message::request_args place_single_args(
            {{"instId", "BTC-USDT"},
             {"tdMode", "cash"},
             {"side", "buy"},
             {"ordType", "limit"},
             {"px", "2.15"},
             {"sz", "2"},
             {"clOrdId", "single" + std::to_string(i)}});
        place_args_array_single.emplace_back(std::move(place_single_args));

        message::request_args cancel_single_args(
            {{"instId", "BTC-USDT"}, {"clOrdId", "single" + std::to_string(i)}});
        cancel_args_array_single.emplace_back(std::move(cancel_single_args));

        message::request_args place_batch_args(
            {{"instId", "BTC-USDT"},
             {"tdMode", "cash"},
             {"side", "buy"},
             {"ordType", "limit"},
             {"px", "2.15"},
             {"sz", "2"},
             {"clOrdId", "batch" + std::to_string(i)}});
        place_args_array_batch.emplace_back(std::move(place_batch_args));

        message::request_args cancel_batch_args(
            {{"instId", "BTC-USDT"}, {"clOrdId", "batch" + std::to_string(i)}});
        cancel_args_array_batch.emplace_back(std::move(cancel_batch_args));
      }

      std::atomic_bool is_start = false;
      std::thread batch_place([&api = or_api, &place_args_array_batch,
                               &cancel_args_array_batch, &is_start] {
        while (!is_start)
          ;
        api.place_orders_ws(place_args_array_batch);
        usleep(100000);
        api.cancel_orders_ws(cancel_args_array_batch);
      });

      std::thread single_place([&api = or_api, &place_args_array_single,
                                &cancel_args_array_single, &is_start] {
        while (!is_start)
          ;

        for (size_t i = 0; i < batch_size; ++i) {
          api.place_order_ws(place_args_array_single[i]);
        }
        usleep(100000);
        for (size_t i = 0; i < batch_size; ++i) {
          api.cancel_order_ws(cancel_args_array_single[i]);
        }
      });
      usleep(500000);
      is_start = true;
      batch_place.join();
      single_place.join();
      usleep(500000);
    };

    sleep(2);
  }
};
}  // namespace collecting_system