#pragma once

#include <bits/types/sig_atomic_t.h>

#include <condition_variable>
#include <mutex>

#include "api_common/constants.hpp"
#include "api/market-data/market_data_api.hpp"
#include "api/market-data/market_data_test_handler.hpp"
#include "metrics/collecting_system/base_collecting_system.hpp"
#include "metrics/metric_collector_creator.hpp"

namespace collecting_system {

using verb = boost::beast::http::verb;

static std::mutex rest_mt;
static std::condition_variable rest_cv;

class simultaneous_collecting_system : public base_collecting_system {
  inline static sig_atomic_t is_running = true;
  inline static sig_atomic_t is_rest_request = false;
  market_data::market_data_api api;

 public:
  simultaneous_collecting_system()
      : api(std::make_unique<market_data::market_data_test_handler>()) {}
  void run() override {
    run_rest_api();
    boost::asio::query(boost::asio::system_executor(),
                       boost::asio::execution::context)
        .join();
  }

 private:
  void run_rest_api() {
    struct sigaction psa;
    psa.sa_handler = simultaneous_collecting_system::http_request_processing;
    sigaction(SIGUSR1, &psa, nullptr);
    sigaction(SIGUSR2, &psa, nullptr);
    std::unique_lock lock(rest_mt);
    while (is_running) {
      rest_cv.wait(lock, [] { return !is_running || is_rest_request; });
      if (!is_running)
        break;
      is_rest_request = false;
      api.get_ticker("/api/v5/market/ticker?instId=BTC-USD-SWAP");
    }
    std::cout << "STOP WORKING: " << stage_filename << std::endl;
    api.stop();
  }

  static void http_request_processing(int signum) {
    std::cout << stage_filename << " handle signal: " << signum << std::endl;
    if (signum == SIGUSR2) {
      is_running = false;
      rest_cv.notify_one();
      return;
    }


    is_rest_request = true;
    rest_cv.notify_one();
  }
};
}  // namespace collecting_system