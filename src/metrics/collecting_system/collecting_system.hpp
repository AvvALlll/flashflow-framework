#pragma once

#include "api/market-data/market_data_api.hpp"
#include "api/market-data/market_data_test_handler.hpp"
#include "metrics/collecting_system/base_collecting_system.hpp"
#include "metrics/metric_collector_creator.hpp"

namespace collecting_system {

class collecting_system : public base_collecting_system {
  market_data::market_data_api api;
 public:
  collecting_system(): api(std::make_unique<market_data::market_data_test_handler>()) {}
  void run() override {
    run_rest_api();
    api.stop();
    boost::asio::query(boost::asio::system_executor(),
                       boost::asio::execution::context)
        .join();
  }

 private:
  void run_rest_api() {
    for (size_t i = 0; i < 100; ++i) {
      api.get_ticker("/api/v5/market/ticker?instId=BTC-USD-SWAP");
      usleep(500000);
    }
  }
};
}  // namespace collecting_system