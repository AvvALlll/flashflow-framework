#pragma once

#include <sys/socket.h>
#include <sys/types.h>
#include <termios.h>

#include "api/market-data/market_data_api.hpp"
#include "api/market-data/market_data_test_handler.hpp"
#include "metrics/collecting_system/base_collecting_system.hpp"
#include "metrics/metric_collector_creator.hpp"
#include "api/order-routing/order_routing_api.hpp"
#include "api/order-routing/order_routing_test_handler.hpp"

namespace collecting_system {

enum msg_type {
  Get = 1,
  Subscribe,
  Unsubscribe,
  WS,
  Post,
  Close,
};

#pragma pack(1)
struct header_t {
  uint8_t type;
  uint8_t api_request;
  uint16_t size;
};

#pragma pack(1)
struct get_request {
  header_t header;
  uint32_t query_size;
};

#pragma pack(1)
struct post_request {
  header_t header;
  uint32_t query_size;
  uint32_t body_size;
};

#pragma pack(1)
struct ws_request {
  header_t header;
  uint8_t op;
  uint32_t args_size;
};

class socket_collecting_system : public base_collecting_system {
  market_data::market_data_api md_api;
  order_routing::order_routing_api or_api;

 public:
  socket_collecting_system()
      : md_api(std::make_unique<market_data::market_data_test_handler>()),
        or_api(std::make_unique<order_routing::order_routing_handler_test>()) {}
  void run() override {
    run_request_handler();
    boost::asio::query(boost::asio::system_executor(),
                       boost::asio::execution::context)
        .join();
  }

 private:
  void run_request_handler() {
    LOG_INFO("start collecting system");
    int sock_fd;
    if (sock_fd = connect_to_server(); sock_fd == -1) {
      return;
    }
    char buffer[8192];
    bool is_running = true;
    while (is_running) {
      memset(buffer, 0, sizeof(buffer));
      int read_size = 0;
      if (read_size = recv(sock_fd, buffer, sizeof(buffer), 0);
          read_size == -1) {
        LOG_ERROR("error in socket read");
        break;
      }
      if (read_size == 0) break;
      int offset = 0;
      while (offset < read_size) {
        header_t* header = reinterpret_cast<header_t*>(buffer + offset);
        if (header->size > read_size - offset) {
          LOG_INFO("read incomplete message");
          memcpy(buffer, buffer + offset, read_size - offset);
          read_size -= offset;
          offset = 0;
          memset(buffer + read_size, 0, sizeof(buffer) - read_size);
          header = reinterpret_cast<header_t*>(buffer + offset);
          read_size += recv(sock_fd, buffer + read_size, header->size - read_size, 0);
        }
        switch (header->type) {
          case msg_type::Get: {
            auto request = reinterpret_cast<get_request*>(buffer + offset);
            if (!md_api.is_market_api((API)request->header.api_request))
              continue;
            std::string query =
                std::string(buffer + offset + sizeof(get_request), request->query_size);
            md_api.get_request((API)request->header.api_request, query,
                               completion_handler);
            offset += sizeof(get_request) + request->query_size;;
            break;
          }
          case msg_type::Post: {
            auto request = reinterpret_cast<post_request*>(buffer + offset);
            std::string query = std::string(buffer + offset + sizeof(post_request), request->query_size);
            std::string args = std::string(buffer + offset + sizeof(post_request) + request->query_size, request->body_size);
            if (md_api.is_market_api((API) request->header.api_request))
              md_api.post_request((API)request->header.api_request, query, args, completion_handler);
            else if (or_api.is_routing_api((API) request->header.api_request))
              or_api.post_request((API)request->header.api_request, query, args, completion_handler);
            offset += sizeof(post_request) + request->query_size + request->body_size;
            break;
          }
          case msg_type::Subscribe:
          case msg_type::Unsubscribe:
          case msg_type::WS: {
            auto request = reinterpret_cast<ws_request*>(buffer + offset);
            std::string args = std::string(buffer + offset + sizeof(ws_request), request->args_size);
            if (md_api.is_market_api((API) request->header.api_request))
              md_api.ws_request((API) request->header.api_request, (Op) request->op, args);
            else if (or_api.is_routing_api((API) request->header.api_request))
              or_api.ws_request((API) request->header.api_request, (Op) request->op, args);
            offset += sizeof(ws_request) + request->args_size;
            break;
          }
          case msg_type::Close: {
            std::cout << "close connection" << std::endl;
            is_running = false;
            offset += sizeof(header_t);
            break;
          } 
          default: {
            LOG_ERROR("[read request error]");
            offset = read_size;
            break;
          }
        }
      }
      
    }
    md_api.stop();
    or_api.stop();
  }

  int connect_to_server() {
    int sock_fd;
    if (sock_fd = socket(PF_INET, SOCK_STREAM, 0); sock_fd < 0) {
      LOG_ERROR("cannot create socket in socket_collecting_system");
      return -1;
    }

    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(9000);
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

    if (connect(sock_fd, (struct sockaddr*)&server_addr, sizeof(server_addr)) <
        0) {
      LOG_ERROR("cannot connect to server");
      return -1;
    }
    return sock_fd;
  }

 private:
  std::function<void(const std::string&)> completion_handler =
      [](const std::string&) {
        // std::cout << response << std::endl;
      };
};
}  // namespace collecting_system