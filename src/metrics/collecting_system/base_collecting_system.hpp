#pragma once

namespace collecting_system {

class base_collecting_system {
 public:
  virtual ~base_collecting_system() {}
  virtual void run() = 0;
};
}  // namespace collecting_system