#pragma once

#include <algorithm>
#include <chrono>
#include <expected>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <iterator>
#include <mutex>
#include <string_view>
#include <vector>

#include "metrics/base_metric_collector.hpp"
#include "utils/error.hpp"
#include "utils/logging.hpp"
#include "utils/utils_function.hpp"

namespace metrics {

struct request_ws_stat_info {
  size_t channel_id;
  uint64_t start_time;
  uint64_t elapsed_time;
  std::string response;

  friend std::fstream& operator<<(std::fstream& file,
                                  const request_ws_stat_info& stat) {
    file << "{"
         << "\"channel_id\":" << stat.channel_id
         << ", \"start_time\":" << stat.start_time
         << ", \"time\":" << stat.elapsed_time
         << ", \"response\":" << stat.response << "}";
    return file;
  }

  friend std::ostream& operator<<(std::ostream& file,
                                  const request_ws_stat_info& stat) {
    file << "\"start_time\":" << stat.start_time
         << " \"time\":" << stat.elapsed_time
         << " \"response\":" << stat.response;
    return file;
  }
};

class request_ws_metric_collector : public base_metric_collector {
  size_t part_id_ = 0;
  std::mutex metric_mt_;
  std::vector<request_ws_stat_info> metric_collection_;

  std::vector<std::mutex> start_points_mt_;
  std::vector<std::deque<std::chrono::system_clock::time_point>> start_points_;

  std::string folder_path_;
  std::string first_name_;
  std::string filename_;

 public:
  request_ws_metric_collector(const std::string& folder_path,
                              const std::string& first_name,
                              const std::string& filename)
      : start_points_mt_(128),
        start_points_(128),
        folder_path_(folder_path),
        first_name_(first_name),
        filename_(filename) {
    std::filesystem::remove_all(folder_path_);
    std::filesystem::create_directory(folder_path_);
  }

  void register_metric(size_t channel_id) override {
    std::scoped_lock lock(start_points_mt_[channel_id]);
    auto start = std::chrono::system_clock::now();
    start_points_[channel_id].push_back(start);
    LOG_TRACE("register metric: ", start_points_[channel_id].back());
  }

  void collect_metric(size_t channel_id, const std::string& response) override {
    std::scoped_lock lock(start_points_mt_[channel_id]);
    if (start_points_[channel_id].empty()) {
      LOG_TRACE("empty start points");
      return;
    }

    auto end = std::chrono::system_clock::now();
    uint64_t ans = std::chrono::duration_cast<std::chrono::nanoseconds>(
                       end - start_points_[channel_id].front())
                       .count();

    std::lock_guard metric_lock(metric_mt_);
    metric_collection_.emplace_back(
        channel_id,
        std::chrono::duration_cast<std::chrono::nanoseconds>(
            std::chrono::time_point_cast<std::chrono::nanoseconds>(
                start_points_[channel_id].front())
                .time_since_epoch())
            .count(),
        ans, response);
    if (metric_collection_.size() >= 10000 && save()) {
      metric_collection_.clear();
    }

    start_points_[channel_id].pop_front();
    LOG_TRACE("register metric: ", metric_collection_.back());
  }

  void roll_back(size_t channel_id) override {
    std::scoped_lock lock(start_points_mt_[channel_id]);
    if (!start_points_[channel_id].empty())
      start_points_[channel_id].pop_front();
  }

  void print_metrics() {
    for (auto stat_info : metric_collection_) {
      std::cout << stat_info << "\n";
    }
  }

  utils::ok_result<void> save() override {
    if (metric_collection_.empty()) return {};

    std::fstream file(folder_path_ + std::to_string(part_id_++) + "_" +
                          first_name_ + "_" + filename_,
                      std::fstream::out);
    if (!file.is_open()) {
      LOG_ERROR("cannot open a file for save");
      return std::unexpected(utils::error{"cannot open file"});
    }
    auto [min_value, max_value] = min_max();
    file << "{\n"
         << R"("min": )" << min_value << ",\n"
         << R"("max": )" << max_value << ",\n"
         << R"("mean": )" << mean() << ",\n"
         << R"("average": )" << utils::average(metric_collection_) << ",\n"
         << R"("measures":[)";
    write_metric_collection(file) << "]\n}\n";

    return {};
  }

  uint64_t mean() const noexcept {
    if (metric_collection_.empty()) return 0;
    auto tmp = metric_collection_;
    std::sort(tmp.begin(), tmp.end(),
              [](auto a, auto b) { return a.elapsed_time < b.elapsed_time; });
    if (tmp.size() & 1) {
      return tmp[(tmp.size() >> 1)].elapsed_time;
    }

    return (tmp[(tmp.size() >> 1) - 1].elapsed_time +
            tmp[(tmp.size() >> 1)].elapsed_time) >>
           1;
  }

  std::pair<uint64_t, uint64_t> min_max() {
    if (metric_collection_.empty()) return {0, 0};
    auto tmp = metric_collection_;
    std::sort(tmp.begin(), tmp.end(),
              [](auto a, auto b) { return a.elapsed_time < b.elapsed_time; });
    return {tmp[0].elapsed_time, tmp.back().elapsed_time};
  }

 private:
  std::fstream& write_metric_collection(std::fstream& file) {
    for (size_t i = 0; i < metric_collection_.size(); ++i) {
      file << metric_collection_[i];
      if (i != metric_collection_.size() - 1) file << ",";
    }
    return file;
  }
};
}  // namespace metrics