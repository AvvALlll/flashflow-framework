#pragma once

#include <boost/asio.hpp>
#include <boost/beast.hpp>
#include <expected>
#include <functional>
#include <memory>
#include <string_view>

#include "boost/asio/any_io_executor.hpp"
#include "utils/error.hpp"
#include "utils/logging.hpp"

namespace network {

namespace net = boost::asio;  // from <boost/asio.hpp>
using tcp = net::ip::tcp;

struct resolve_info {
  std::string host;
  std::string port;
  std::string path;
  std::string query;
  std::string full_host;

  const std::string& get_full_host(std::string port) { // TODO: shadow port
    full_host = host + ':' + port;
    return full_host;
  }
  const std::string& get_full_host() {
    full_host = host + ':' + port;
    return full_host;
  }

  std::string full_path() const { return path + "?" + query; }
};

class resolver {
  net::ip::tcp::resolver resolver_;

 public:
  resolver(net::strand<net::any_io_executor>& ioc) : resolver_(ioc) {}

  std::expected<net::ip::basic_resolver_results<boost::asio::ip::tcp>,
                utils::error>

  resolve(const resolve_info& info) {
    auto const results = resolver_.resolve(info.host, info.port);
    if (results.empty()) {
      LOG_ERROR("[cannot resolve] host: ", info.host, "port=", info.port);
      return std::unexpected(utils::error{"[cannot resolve] host: ", info.host,
                                          " port: ", info.port});
    }
    return results;
  }

  auto async_resolve(
      const resolve_info& info,
      const std::function<void(boost::system::error_code,
                               tcp::resolver::results_type)>& on_resolve) {
    return resolver_.async_resolve(info.host, info.port, on_resolve);
  }
};
}  // namespace network