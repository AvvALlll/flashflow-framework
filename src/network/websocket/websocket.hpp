#pragma once

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/asio/ssl/stream.hpp>
#include <boost/beast.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/core/stream_traits.hpp>
#include <boost/beast/core/tcp_stream.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/websocket/ssl.hpp>
#include <concepts>
#include <expected>
#include <format>
#include <functional>
#include <future>
#include <iostream>
#include <memory>

#include "boost/asio/any_io_executor.hpp"
#include "boost/beast/core/bind_handler.hpp"
#include "boost/beast/core/buffers_to_string.hpp"
#include "boost/beast/core/error.hpp"
#include "boost/system/detail/error_code.hpp"
#include "network/resolver.hpp"
#include "utils/error.hpp"
#include "utils/logging.hpp"

namespace network {

using namespace boost::beast::websocket;
using tcp = boost::asio::ip::tcp;  // from <boost/asio/ip/tcp.hpp>
namespace beast = boost::beast;
namespace net = boost::asio;       // from <boost/asio.hpp>
namespace ssl = boost::asio::ssl;  // from <boost/asio/ssl.hpp>

class websocket;

using rw_ws_completion_handler_t = std::function<void(
    std::shared_ptr<websocket>, boost::system::error_code, std::size_t)>;

using ws_result = utils::ok_result<void>;
using connect_handler_t = std::function<void(std::shared_ptr<websocket>)>;
using error_handler_t = std::function<void(boost::system::error_code)>;

class websocket : public std::enable_shared_from_this<websocket> {
  net::strand<net::any_io_executor> ex_;
  net::ssl::context& ctx_;
  resolver resolver_;

  using ws_ptr = std::unique_ptr<stream<beast::ssl_stream<beast::tcp_stream>>>;
  ws_ptr wss_;
  boost::beast::multi_buffer buffer_;

  resolve_info endpoint_info_;

 public:
  websocket(const websocket& wss) = delete;

  explicit websocket(net::any_io_executor ex, net::ssl::context& ctx)
      : ex_(net::make_strand(ex)), ctx_(ctx), resolver_(ex_) {}

  void async_start(
      const resolve_info& info, connect_handler_t connect_handler,
      error_handler_t error_handler = [](auto) {}) {
    wss_ = std::make_unique<stream<beast::ssl_stream<beast::tcp_stream>>>(ex_,
                                                                          ctx_);
    endpoint_info_ = info;
    resolver_.async_resolve(
        endpoint_info_,
        beast::bind_front_handler(&websocket::on_resolve, shared_from_this(),
                                  connect_handler, error_handler));
  }

  ws_result start(const resolve_info& info) {
    wss_ = std::make_unique<stream<beast::ssl_stream<beast::tcp_stream>>>(
        net::make_strand(ex_), ctx_);
    endpoint_info_ = info;
    return resolver_.resolve(info).and_then([&](auto&& results) -> ws_result {
      auto ep = get_lowest_layer(*wss_).connect(results);

      if (!SSL_set_tlsext_host_name(wss_->next_layer().native_handle(),
                                    endpoint_info_.host.data())) {
        LOG_ERROR("Failed to set SNI Hostname");
        return std::unexpected(utils::error("Failed to set SNI Hostname"));
      }

      wss_->next_layer().handshake(ssl::stream_base::client);
      // todo: error handling
      wss_->handshake(endpoint_info_.get_full_host(std::to_string(ep.port())),
                      endpoint_info_.full_path());
      return {};
    });
  }

  ws_result write(std::string_view data) {
    return write_buffer(net::buffer(data));
  }

  ws_result read() {
    buffer_.clear();
    try {
      wss_->read(buffer_);
    } catch (const std::exception& err) {
      return std::unexpected(
          utils::error{"[websocket read]", std::string_view(err.what())});
    }
    return {};
  }

  void async_write(const std::string& data,
                   rw_ws_completion_handler_t completion_handler) {
    wss_->async_write(
        net::buffer(data),
        beast::bind_front_handler(completion_handler, shared_from_this()));
  }

  void async_read(rw_ws_completion_handler_t completion_handler) {
    buffer_.clear();
    wss_->async_read(buffer_, beast::bind_front_handler(completion_handler,
                                                        shared_from_this()));
  }

  template <typename F>
  requires(requires(F&& f) { f(); }) void execute_async_op(F&& f) {
    post(ex_, std::forward<F>(f));
  }

  std::string str_data() const noexcept {
    return boost::beast::buffers_to_string(buffer_.data());
  }

  const boost::beast::multi_buffer& buffer() const noexcept { return buffer_; }

  bool is_open() const noexcept {
    auto package = std::make_shared<std::packaged_task<bool()>>(
        [&wss = wss_] { return wss && wss->is_open(); });
    std::future<bool> future = package->get_future();
    post(ex_, [package] mutable { (*package)(); });
    return future.get();
  }

  void async_close() {
    if (is_open()) {
      LOG_DEBUG("[async close]");
      wss_->async_close(
          close_code::normal,
          beast::bind_front_handler(&websocket::on_close, shared_from_this()));
    }
  }

  void close() {
    LOG_DEBUG("[close]");
    if (is_open()) wss_->close(close_code::normal);
  }

  void async_ping() {
    wss_->async_ping("ping", [](auto error_code){
      if (error_code) {
        LOG_ERROR("[ping] ", error_code.message());
      }
    });
  }

 private:
  void on_resolve(connect_handler_t connect_handler,
                  error_handler_t error_handler, boost::system::error_code ec,
                  tcp::resolver::results_type results) {
    if (ec) {
      LOG_ERROR("[resolve] ", ec.message());
      error_handler(ec);
      return;
    }
    get_lowest_layer(*wss_).async_connect(
        results,
        beast::bind_front_handler(&websocket::on_connect, shared_from_this(),
                                  connect_handler, error_handler));
  }

  void on_connect(connect_handler_t connect_handler,
                  error_handler_t error_handler, beast::error_code ec,
                  tcp::resolver::results_type::endpoint_type ep) {
    if (ec) {
      LOG_ERROR("[connect] ", ec.message());
      error_handler(ec);
      return;
    }

    if (!SSL_set_tlsext_host_name(wss_->next_layer().native_handle(),
                                  endpoint_info_.host.data())) {
      ec = beast::error_code(static_cast<int>(::ERR_get_error()),
                             net::error::get_ssl_category());
      LOG_ERROR("[ssl_connect] ", ec.message());
      return;
    }

    endpoint_info_.get_full_host(std::to_string(ep.port()));
    wss_->next_layer().async_handshake(
        ssl::stream_base::client,
        beast::bind_front_handler(&websocket::on_ssl_handshake,
                                  shared_from_this(), connect_handler,
                                  error_handler));
  }

  void on_ssl_handshake(connect_handler_t connect_handler,
                        error_handler_t error_handler, beast::error_code ec) {
    if (ec) {
      LOG_ERROR("[ssl_handshake] ", ec.message());
      error_handler(ec);
      return;
    }

    wss_->async_handshake(
        endpoint_info_.get_full_host(), endpoint_info_.full_path(),
        beast::bind_front_handler(&websocket::on_handshake, shared_from_this(),
                                  connect_handler, error_handler));
  }

  void on_handshake(connect_handler_t connect_handler,
                    error_handler_t error_handler, beast::error_code ec) {
    if (ec) {
      LOG_ERROR("[handshake] ", ec.message());
      error_handler(ec);
      return;
    }

    connect_handler(shared_from_this());
  }

  void on_close(beast::error_code ec) {
    if (ec) {
      LOG_ERROR("[close] ", ec.message());
      return;
    }
  }

  template <typename Buffer>
  requires(net::is_const_buffer_sequence<Buffer>::value) ws_result
      write_buffer(Buffer&& buffer) {
    try {
      wss_->write(std::forward<Buffer>(buffer));
    } catch (const std::exception& err) {
      return std::unexpected(
          utils::error{"[websocket write] ", std::string_view(err.what())});
    }
    return {};
  }
};
}  // namespace network