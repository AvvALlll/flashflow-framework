#pragma once

#include <atomic>
#include <list>
#include <set>

#include "message/base_message.hpp"
#include "message/json_handler.hpp"
#include "network/websocket/websocket.hpp"

namespace network {

class websocket_channel {
  const size_t WAIT_TIME_N = 25;
  std::shared_ptr<websocket> ws_;

  resolve_info info_;
  rw_ws_completion_handler_t read_completion_;

  std::list<message::ws_request_message> send_message_queue_;
  std::set<message::ws_request_message> sub_message_queue_;

  using listener_t = std::function<void(const std::string&)>;
  using prestart_t = std::function<void()>;
  listener_t listener_;
  prestart_t prestart_;

  std::atomic_bool is_reading_ = false;
  std::atomic_bool is_writing_ = false;

  message::json_handler json_handler_;

  std::mutex timer_mt_;
  boost::asio::steady_timer timer_;

  size_t channel_id_ = 0;

 public:
  explicit websocket_channel(net::any_io_executor ex, net::ssl::context& ctx,
                             resolve_info info)
      : ws_(std::make_shared<websocket>(ex, ctx)),
        info_(info),
        timer_(ex, boost::asio::chrono::seconds(WAIT_TIME_N)) {
    static size_t channel_id = 0;
    channel_id_ = ++channel_id;
    start();
    start_listen([](auto) {});
    timer_.async_wait(
        beast::bind_front_handler(&websocket_channel::timer_tick, this));
  }

  void set_listener(listener_t listener) { listener_ = listener; }
  void set_prestart(prestart_t prestart) { prestart_ = prestart; }
  size_t channel_id() const noexcept {
    return channel_id_;
  }

  ws_result start() { return ws_->start(info_); }

  void async_start() {
    return ws_->async_start(info_, [](auto) {});
  }
  ws_result send(message::ws_request_message&& ws_request,
                 error_handler_t error_handler) {
    if (!ws_->is_open()) {
      start();
      if (prestart_)
        prestart_();
      start_listen(error_handler);
    }
    auto&& message = ws_request.create_message();
    return ws_->write(message);
  }

  void async_send(
      message::ws_request_message&& ws_request,
      error_handler_t error_handler = [](auto) {}) {
    LOG_TRACE("ws ", channel_id_, ": ", ws_request.create_message());
    if (ws_request.is_subscribe()) {
      sub_message_queue_.insert(ws_request);
    } else if (ws_request.is_unsubscribe()) {
      std::erase_if(sub_message_queue_, [&ws_request](const auto& msg) {
        return msg.args() == ws_request.args();
      });
    }

    if (!ws_->is_open()) {
      send_message_queue_.push_back(
          std::forward<message::ws_request_message>(ws_request));
    } else {
      common_async_send(std::forward<message::ws_request_message>(ws_request),
                        error_handler);
    }
  }

  void async_send(
      const message::ws_request_message& ws_request,
      error_handler_t error_handler = [](auto) {}) {
    LOG_TRACE("ws", channel_id_, ":", ws_request.create_message());        
    if (ws_request.is_subscribe()) {
      sub_message_queue_.insert(ws_request);
    } else if (ws_request.is_unsubscribe()) {
      std::erase_if(sub_message_queue_, [&ws_request](const auto& msg) {
        return msg.args() == ws_request.args();
      });
    }

    if (!ws_->is_open()) {
      send_message_queue_.push_back(ws_request);
    } else {
      common_async_send(ws_request, error_handler);
    }
  }

  void start_listen(error_handler_t error_handler = [](auto) {}) {
    LOG_TRACE("start reading");
    read_completion_ = [this, error_handler](std::shared_ptr<websocket> ws,
                                             beast::error_code ec,
                                             std::size_t) {
      if (ec && ec != net::error::operation_aborted) {
        LOG_ERROR("[ws channel", channel_id_, "read] ", ec.message());
        restart(error_handler);
        return;
      } else if (ec) {
        return;
      }

      timer_restart();
      auto data = ws->str_data();
      data_handling(data.c_str());

      if (is_reading_) {
        ws->async_read(read_completion_);
      }
    };

    is_reading_ = true;
    ws_->async_read(read_completion_);
  }

  void stop_listen() {
    is_reading_ = false;
    is_writing_ = false;
    ws_->execute_async_op(std::bind(&websocket_channel::clear, this));
  }

  void close() {
    stop_listen();
    ws_->close();
  }

  void async_close() {
    stop_listen();
    ws_->async_close();
  }

 private:
  void timer_tick(beast::error_code ec) {
    if (ec) {
      LOG_TRACE("[ws timer]: ", ec.message());
      return;
    }
    std::lock_guard lock(timer_mt_);
    timer_.cancel();
    timer_.expires_from_now(boost::asio::chrono::seconds(WAIT_TIME_N));
    ws_->async_ping();
    timer_.async_wait(
        beast::bind_front_handler(&websocket_channel::timer_tick, this));
  }

  void timer_restart() {
    std::lock_guard lock(timer_mt_);
    timer_.cancel();
    timer_.expires_from_now(boost::asio::chrono::seconds(WAIT_TIME_N));
    timer_.async_wait(
        beast::bind_front_handler(&websocket_channel::timer_tick, this));
  }

  void clear() {
    std::lock_guard lock(timer_mt_);
    timer_.cancel();
    sub_message_queue_.clear();
    send_message_queue_.clear();
  }

  void common_async_send(
      message::ws_request_message&& ws_request,
      error_handler_t error_handler = [](auto) {}) {
    auto sub_handler =
        [this, request = std::forward<message::ws_request_message>(ws_request),
         error_handler] mutable {
          if (is_writing_) {
            send_message_queue_.push_back(
                std::forward<message::ws_request_message>(request));
            return;
          }

          is_writing_ = true;
          auto&& message = request.create_message();
          ws_->async_write(message, [this, error_handler](
                                        std::shared_ptr<websocket>,
                                        beast::error_code ec, std::size_t) {
            if (ec && ec != boost::asio::error::operation_aborted) {
              LOG_ERROR("[ws channel send] ", ec.message());
              error_handler(ec);

              if (!ws_->is_open()) {
                restart(error_handler);
              }
              return;
            } else if (ec) {
              error_handler(ec);
              return;
            }
            send_next_request(error_handler);
            is_writing_ = false;
          });
        };

    ws_->execute_async_op(std::move(sub_handler));
  }

  void common_async_send(
      const message::ws_request_message& ws_request,
      error_handler_t error_handler = [](auto) {}) {
    auto sub_handler = [this, ws_request, error_handler] mutable {
      if (is_writing_) {
        send_message_queue_.push_back(ws_request);
        return;
      }

      is_writing_ = true;
      auto&& message = ws_request.create_message();
      ws_->async_write(
          message, [this, error_handler](std::shared_ptr<websocket>,
                                         beast::error_code ec, std::size_t) {
            if (ec) {
              LOG_ERROR("[ws channel asyn sub] ", ec.message());
              error_handler(ec);

              if (!ws_->is_open()) {
                restart(error_handler);
              }
              return;
            }

            send_next_request(error_handler);
            is_writing_ = false;
          });
    };

    ws_->execute_async_op(std::move(sub_handler));
  }

  void data_handling(const std::string& data) {
    auto&& doc = json_handler_.parse(data);
    if (!doc.has_value()) {
      LOG_DEBUG(doc.error());
      return;
    }
    auto event =
        doc.and_then([](auto&& obj) { return obj.next_string("event"); });
    if (event) {
      auto msg =
          doc.and_then([](auto&& obj) { return obj.next_string("msg"); });

      if (event.value() == "error")
        LOG_ERROR("[data handling] event error: ", msg.value());
    }
    if (listener_) listener_(data);
  }

  void restart(error_handler_t error_handler) {
    ws_->async_start(info_, [this, error_handler](auto) mutable {
      if (prestart_)
        prestart_();
      timer_restart();
      start_listen(error_handler);
      for (auto& sub_args: sub_message_queue_) {
        send_message_queue_.push_front(sub_args);
      }
      send_next_request(error_handler);
    });
  }

  void send_next_request(error_handler_t error_handler) {
    if (!send_message_queue_.empty()) {
      auto tmp_args_queue = std::move(send_message_queue_.front());
      send_message_queue_.pop_front();
      common_async_send(std::move(tmp_args_queue), error_handler);
      return;
    }
  }
};
}  // namespace network