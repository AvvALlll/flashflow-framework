#pragma once

#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/error.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/version.hpp>
#include <deque>
#include <functional>
#include <iterator>
#include <map>
#include <optional>
#include <thread>

#include "boost/asio/any_io_executor.hpp"
#include "boost/asio/io_service.hpp"
#include "boost/asio/ssl/stream.hpp"
#include "boost/beast/core/stream_traits.hpp"
#include "boost/beast/http/empty_body.hpp"
#include "network/resolver.hpp"

namespace network {

namespace beast = boost::beast;  // from <boost/beast.hpp>
namespace http = beast::http;    // from <boost/beast/http.hpp>
namespace net = boost::asio;     // from <boost/asio.hpp>
namespace ssl = net::ssl;        // from <boost/asio/ssl.hpp>
using tcp = net::ip::tcp;        // from <boost/asio/ip/tcp.hpp>

class http_service;

using request_t = http::request<http::string_body>;
using response_t = http::response<http::string_body>;

using read_http_completion_handler_t = std::function<void(const response_t&)>;
using write_http_completion_handler_t =
    std::function<void(std::shared_ptr<http_service>)>;
using error_handler_t = std::function<void(beast::error_code)>;

using headers_t = const std::map<std::string, std::string>;

class http_request_args {
  http::verb method_;
  std::string target_;
  std::optional<std::string> body_;
  std::optional<headers_t> headers_;

 public:
  http_request_args(http::verb method, std::string_view target)
      : method_(method), target_(target){};
  http_request_args(http::verb method, std::string_view target,
                    const headers_t& headers)
      : method_(method), target_(target), headers_(headers){};

  http_request_args(http::verb method, std::string_view target,
                    headers_t&& headers)
      : method_(method), target_(target), headers_(std::move(headers)){};

  http_request_args(http::verb method, std::string target,
                    std::string_view body)
      : method_(method), target_(target), body_(body) {}
  http_request_args(http::verb method, std::string target,
                    const headers_t& headers, std::string_view body)
      : method_(method), target_(target), body_(body), headers_(headers) {}

  http_request_args(http::verb method, std::string target, headers_t&& headers,
                    std::string_view body)
      : method_(method),
        target_(target),
        body_(body),
        headers_(std::move(headers)) {}

  constexpr bool has_body() const noexcept { return body_.has_value(); }
  constexpr bool has_headers() const noexcept { return headers_.has_value(); }

  constexpr http::verb method() const noexcept { return method_; }

  constexpr std::string target() const noexcept { return target_; }

  constexpr std::string body() const noexcept { return body_.value(); }

  constexpr const headers_t& headers() const noexcept {
    return headers_.value();
  }
};

class http_service : public std::enable_shared_from_this<http_service> {
  struct request_args_t {
    http_request_args args;
    write_http_completion_handler_t completion_handler;
    error_handler_t error_handler;
  };

  struct read_args_t {
    read_http_completion_handler_t completion_handler;
    error_handler_t error_handler;
  };

  net::strand<net::any_io_executor> ex_;
  net::ssl::context& ctx_;
  resolver resolver_;
  std::unique_ptr<ssl::stream<beast::tcp_stream>> stream_;
  beast::flat_buffer buffer_;

  request_t request_;
  response_t response_;
  std::shared_ptr<resolve_info> endpoint_info_;

  std::deque<request_args_t> request_args_queue_;
  std::deque<request_args_t> read_request_args_queue_;
  std::deque<read_args_t> read_handler_queue_;

  // std::atomic_size_t write_count_{0};
  // std::atomic_size_t read_count_{0};
 public:
  explicit http_service(net::any_io_executor ex, net::ssl::context& ctx)
      : ex_(net::make_strand(ex)), ctx_(ctx), resolver_(ex_) {}

  utils::ok_result<void> start(std::shared_ptr<resolve_info> info) {
    stream_ = std::make_unique<ssl::stream<beast::tcp_stream>>(ex_, ctx_);
    endpoint_info_ = info;
    if (!SSL_set_tlsext_host_name(stream_->native_handle(),
                                  endpoint_info_->host.data())) {
      LOG_ERROR("Failed to set SNI Hostname");
      return std::unexpected(utils::error("Failed to set SNI Hostname"));
    }

    return resolver_.resolve(*endpoint_info_)
        .and_then([&](auto&& results) -> utils::ok_result<void> {
          beast::get_lowest_layer(*stream_).connect(results);
          stream_->handshake(ssl::stream_base::client);
          return {};
        });
  }

  void async_start(std::shared_ptr<resolve_info> info,
                   write_http_completion_handler_t completion_handler) {
    stream_ = std::make_unique<ssl::stream<beast::tcp_stream>>(ex_, ctx_);
    endpoint_info_ = info;
    if (!SSL_set_tlsext_host_name(stream_->native_handle(),
                                  endpoint_info_->host.data())) {
      LOG_ERROR("Failed to set SNI Hostname");
      return;
    }

    resolver_.async_resolve(
        *endpoint_info_,
        beast::bind_front_handler(&http_service::on_resolve, shared_from_this(),
                                  completion_handler));
  }

  bool is_open() const noexcept {
    return stream_ && stream_->lowest_layer().is_open();
  }

  void send_request(const http_request_args& args) {
    set_property(args);
    http::write(*stream_, request_);
  }

  template <typename Args = http_request_args>
  void async_send_request(Args&& args,
                          write_http_completion_handler_t completion_handler,
                          error_handler_t error_handler) {
    LOG_TRACE("[async write]");
    auto handler = [this, args, completion_handler, error_handler]() {
      request_args_queue_.emplace_back(args, completion_handler, error_handler);
      if (request_args_queue_.size() > 1) return;

      set_property(std::move(args));
      http::async_write(
          *stream_, request_,
          net::bind_executor(ex_,
                             beast::bind_front_handler(
                                 &http_service::on_write, shared_from_this(),
                                 completion_handler, error_handler)));
    };
    post(ex_, handler);
  }

  response_t read_response() {
    buffer_.clear();
    response_ = {};
    http::read(*stream_, buffer_, response_);
    return response_;
  }

  void async_read_response(read_http_completion_handler_t completion_handler,
                           error_handler_t error_handler) {
    LOG_TRACE("[async read]\n");
    auto handler = [this, completion_handler, error_handler]() {
      read_handler_queue_.emplace_back(completion_handler, error_handler);
      if (read_handler_queue_.size() > 1) return;
      if (!is_open()) std::cout << "read with close stream\n";
      http::async_read(*stream_, buffer_, response_,
                       net::bind_executor(
                           ex_, beast::bind_front_handler(
                                    &http_service::on_read, shared_from_this(),
                                    completion_handler, error_handler)));
    };
    post(ex_, handler);
  }

  void close() {
    auto close_handler = [this]() {
      if (is_open()) stream_->next_layer().close();
    };
    post(ex_, close_handler);
  }

 private:
  template <typename Args = http_request_args>
  void set_property(Args&& args) {
    request_.clear();
    request_.method(args.method());
    request_.keep_alive(true);
    request_.target(args.target());
    request_.set(http::field::host, endpoint_info_->host);
    request_.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);

    if (args.has_headers()) {
      for (auto&& [key, value] : args.headers()) {
        request_.set(key, value);
      }
    }

    if (args.has_body()) {
      request_.set(beast::http::field::content_type, "application/json");
      request_.body() = args.body();
      request_.prepare_payload();
    }
  }

  void set_property(const http_request_args& args) {
    request_.clear();
    request_.method(args.method());
    request_.target(args.target());
    request_.set(http::field::host, endpoint_info_->host);
    request_.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);

    if (args.has_headers()) {
      for (auto& [key, value] : args.headers()) {
        request_.set(key, value);
      }
    }

    if (args.has_body()) {
      request_.set(beast::http::field::content_type, "application/json");
      request_.body() = args.body();
      request_.prepare_payload();
    }
  }

  void on_resolve(write_http_completion_handler_t completion_handler,
                  beast::error_code ec, tcp::resolver::results_type results) {
    if (ec) {
      LOG_ERROR("[http resolve] ", ec.message());
      return;
    }

    beast::get_lowest_layer(*stream_).async_connect(
        results,
        beast::bind_front_handler(&http_service::on_connect, shared_from_this(),
                                  completion_handler));
  }

  void on_connect(write_http_completion_handler_t completion_handler,
                  beast::error_code ec,
                  tcp::resolver::results_type::endpoint_type) {
    if (ec) {
      LOG_ERROR("[http connect] ", ec.message());
      return;
    }
    stream_->async_handshake(
        ssl::stream_base::client,
        beast::bind_front_handler(&http_service::on_handshake,
                                  shared_from_this(), completion_handler));
  }

  void on_handshake(write_http_completion_handler_t completion_handler,
                    beast::error_code ec) {
    if (ec) {
      LOG_ERROR("[http handshake] ", ec.message());
      return;
    }
    completion_handler(shared_from_this());
  }

  void on_write(write_http_completion_handler_t completion_handler,
                error_handler_t error_handler, beast::error_code ec,
                std::size_t wrote_bytes) {
    if (wrote_bytes == 0) {
      LOG_DEBUG("[on write] zero write");
    }
    LOG_TRACE("[http write]: ", wrote_bytes);

    if (ec == beast::error::timeout) {
      LOG_ERROR("[http write] ", ec.message());
      close();
      async_start(endpoint_info_,
                  [](auto http_service) { http_service->async_write_front(); });
      return;
    } else if (ec) {
      LOG_ERROR("[http write] ", ec.message());
      error_handler(ec);

      close();
      read_request_args_queue_.clear();
      read_handler_queue_.clear();
      request_args_queue_.clear();
      return;
    }
    if (!request_args_queue_.empty()) {
      read_request_args_queue_.push_back(
          std::move(request_args_queue_.front()));
      request_args_queue_.pop_front();
    }

    // LOG_DEBUG("write cnt", write_count_++);
    completion_handler(shared_from_this());
    async_write_front();
  }

  void async_write_front() {
    LOG_TRACE("[async write front]\n");
    auto handler = [this]() {
      if (!request_args_queue_.empty()) {
        auto [args, next_completion_handler, error_handler] =
            request_args_queue_.front();
        set_property(args);
        http::async_write(
            *stream_, request_,
            net::bind_executor(ex_,
                               beast::bind_front_handler(
                                   &http_service::on_write, shared_from_this(),
                                   next_completion_handler, error_handler)));
      }
    };
    handler();
  }

  void on_read(read_http_completion_handler_t completion_handler,
               error_handler_t error_handler, beast::error_code ec,
               std::size_t read_data) {
    if (read_data == 0) {
      LOG_DEBUG("[on read] zero read");
    }
    LOG_TRACE("[on read]");
    if (ec == beast::error::timeout || ec == http::error::end_of_stream) {
      LOG_ERROR("[http read] ", ec.message());
      read_handler_queue_.clear();
      close();
      async_start(
          endpoint_info_,
          [&read_request_args_queue = read_request_args_queue_,
           &request_args_queue = request_args_queue_](auto http_service) {
            for (auto& args: read_request_args_queue) {
              request_args_queue.push_back(std::move(args));
            }
            read_request_args_queue.clear();
            http_service->async_write_front();
          });
      return;
    } else if (ec) {
      LOG_ERROR("[http read unregister] ", ec.message());
      error_handler(ec);

      request_args_queue_.clear();
      read_handler_queue_.clear();
      read_request_args_queue_.clear();
      close();
      return;
    }
    // LOG_DEBUG("read cnt: ", read_count_++);
    if (!read_request_args_queue_.empty()) read_request_args_queue_.pop_front();
    if (!read_handler_queue_.empty()) read_handler_queue_.pop_front();
    beast::get_lowest_layer(*stream_).expires_after(std::chrono::seconds(10));
    completion_handler(response_);

    response_ = {};
    buffer_.clear();
    async_read_front();
  }

  void async_read_front() {
    LOG_TRACE("[async read front]\n");
    auto handler = [this]() {
      if (!read_handler_queue_.empty()) {
        auto& [read_completion_handler, error_handler] =
            read_handler_queue_.front();
        http::async_read(
            *stream_, buffer_, response_,
            net::bind_executor(ex_,
                               beast::bind_front_handler(
                                   &http_service::on_read, shared_from_this(),
                                   read_completion_handler, error_handler)));
      }
    };
    handler();
  }

  void on_shutdown(beast::error_code ec) {
    LOG_TRACE("shutdown http service");
    if (ec == net::error::eof) {
      ec = {};
    }
  }
};
}  // namespace network