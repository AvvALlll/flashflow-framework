#pragma once

#include <string>
#include <iostream>

#include "api/order-routing/order_routing_handler_base.hpp"

namespace order_routing {
class order_routing_handler_test : public order_routing_handler_base {
  void on_place_order(const std::string&) override{
    // printf("%s\n", response.c_str());
  };

  void on_place_batch_orders(const std::string&) override {}
  void on_cancel_order(const std::string&) override{
    // printf("%s\n", response.c_str());
  };
  void on_cancel_batch_orders(const std::string&) override {}

  void on_amend_order(const std::string&) override {
      // printf("%s\n", response.c_str());
  }

  void on_amend_batch_orders(const std::string&) override {}

  void on_order_channel(const std::string&) override {
    // printf("%s\n", response.c_str());
  };

  void on_place_order_ws(const std::string&) override { 
    // printf("%s\n", response.c_str());
  }
  void on_place_batch_orders_ws(const std::string&) override {
    // printf("%s\n", response.c_str());
  }
  void on_cancel_order_ws(const std::string&) override { 
    // printf("%s\n", response.c_str());
  }
  void on_cancel_batch_orders_ws(const std::string&) override {
    // printf("%s\n", response.c_str());
  }

  void on_amend_order_ws(const std::string&) override {}

  void on_amend_batch_orders_ws(const std::string&) override {}
};
}  // namespace order_routing