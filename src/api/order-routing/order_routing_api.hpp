#pragma once
#include <type_traits>
#include <unordered_map>

#include "api/order-routing/order_routing_handler_base.hpp"
#include "api_common/api_methods.hpp"
#include "api_common/constants.hpp"
#include "gateway/gateway_service_manager.hpp"
#include "gateway/request_api/base_request.hpp"
#include "gateway/request_api/http_request.hpp"
#include "gateway/request_api/ws_request.hpp"
#include "message/base_message.hpp"
#include "network/http/http_service.hpp"
#include "utils/algo.hpp"
#include "utils/utils_function.hpp"

namespace order_routing {

namespace http = boost::beast::http;

class order_routing_api {
  metrics::metric_collector_creator collector_creator;
  gateway::gateway_service_manager gateway_manager;
  std::unique_ptr<order_routing_handler_base> api_handler_;

  std::unordered_map<API, std::shared_ptr<request::base_request>> requests = {
      {API::Place_order,
       std::make_shared<request::http_request<API::Place_order>>(
           gateway_manager, collector_creator)},
      {API::Place_batch_orders,
       std::make_shared<request::http_request<API::Place_batch_orders>>(
           gateway_manager, collector_creator)},
      {API::Cancel_order,
       std::make_shared<request::http_request<API::Cancel_order>>(
           gateway_manager, collector_creator)},
      {API::Cancel_batch_orders,
       std::make_shared<request::http_request<API::Cancel_batch_orders>>(
           gateway_manager, collector_creator)},
      {API::Amend_order,
       std::make_shared<request::http_request<API::Amend_order>>(
           gateway_manager, collector_creator)},
           {API::Amend_batch_orders,
       std::make_shared<request::http_request<API::Amend_batch_orders>>(
           gateway_manager, collector_creator)},
      {API::Order_channel,
       std::make_shared<request::ws_request<API::Order_channel>>(
           gateway_manager,
           std::bind(&order_routing_handler_base::on_order_channel,
                     api_handler_.get(), std::placeholders::_1),
           collector_creator)},
      {API::Place_order_ws,
       std::make_shared<request::ws_request<API::Place_order_ws>>(
           gateway_manager,
           std::bind(&order_routing_handler_base::on_place_order_ws,
                     api_handler_.get(), std::placeholders::_1),
           collector_creator)},
      {API::Place_batch_orders_ws,
       std::make_shared<request::ws_request<API::Place_batch_orders_ws>>(
           gateway_manager,
           std::bind(&order_routing_handler_base::on_place_batch_orders_ws,
                     api_handler_.get(), std::placeholders::_1),
           collector_creator)},
      {API::Cancel_order_ws,
       std::make_shared<request::ws_request<API::Cancel_order_ws>>(
           gateway_manager,
           std::bind(&order_routing_handler_base::on_cancel_order_ws,
                     api_handler_.get(), std::placeholders::_1),
           collector_creator)},
      {API::Cancel_batch_orders_ws,
       std::make_shared<request::ws_request<API::Cancel_batch_orders_ws>>(
           gateway_manager,
           std::bind(&order_routing_handler_base::on_cancel_batch_orders_ws,
                     api_handler_.get(), std::placeholders::_1),
           collector_creator)},
      {API::Amend_order_ws,
       std::make_shared<request::ws_request<API::Amend_order_ws>>(
           gateway_manager,
           std::bind(&order_routing_handler_base::on_amend_order_ws,
                     api_handler_.get(), std::placeholders::_1),
           collector_creator)},
      {API::Amend_batch_orders_ws,
       std::make_shared<request::ws_request<API::Amend_batch_orders_ws>>(
           gateway_manager,
           std::bind(&order_routing_handler_base::on_amend_batch_orders_ws,
                     api_handler_.get(), std::placeholders::_1),
           collector_creator)},
  };

 public:
  explicit order_routing_api(
      std::unique_ptr<order_routing_handler_base> api_handler)
      : collector_creator(metric_folder_path, stage_filename),
        api_handler_(std::move(api_handler)) {}

  void reset_handler(std::unique_ptr<order_routing_handler_base> new_handler) {
    api_handler_ = std::move(new_handler);
  }

  void stop() {
    for (auto& [request_id, request] : requests) {
      request->stop();
    }
  }

  void place_order(const message::request_args& args) {
    std::string body = args.args_format();
    inner_place_order(std::move(body));
  }

  void place_order(message::request_args&& args) {
    std::string body = args.args_format();
    inner_place_order(std::move(body));
  }

  void place_orders(const message::request_args& args) {
    std::string body = args.args_format();
    inner_place_orders(std::move(body));
  }

  void place_orders(message::request_args&& args) {
    std::string body = args.args_format();
    inner_place_orders(std::move(body));
  }

  void cancel_order(const message::request_args& args) {
    std::string body = args.args_format();
    inner_cancel_order(std::move(body));
  }

  void cancel_order(message::request_args&& args) {
    std::string body = args.args_format();
    inner_cancel_order(std::move(body));
  }

  void cancel_orders(const message::request_args& args) {
    std::string body = args.args_format();
    inner_cancel_orders(std::move(body));
  }

  void cancel_orders(message::request_args&& args) {
    std::string body = args.args_format();
    inner_cancel_orders(std::move(body));
  }

  void amend_order(const message::request_args& args) {
    std::string body = args.args_format();
    inner_amend_order(std::move(body));
  }

  void amend_order(message::request_args&& args) {
    std::string body = args.args_format();
    inner_amend_order(std::move(body));
  }

  void amend_orders(const message::request_args& args) {
    std::string body = args.args_format();
    inner_amend_orders(std::move(body));
  }

  void amend_orders(message::request_args&& args) {
    std::string body = args.args_format();
    inner_amend_orders(std::move(body));
  }

  void place_order_ws(const message::request_args& args) {
    message::ws_request_message request("placews", op_to_str(Op::Order_op),
                                        args.args_format());
    requests[API::Place_order_ws]->ws_execute(std::move(request));
  }
  void place_order_ws(message::request_args&& args) {
    message::ws_request_message request("placews", op_to_str(Op::Order_op),
                                        args.args_format());
    requests[API::Place_order_ws]->ws_execute(std::move(request));
  }

  void place_orders_ws(const std::vector<message::request_args>& args) {
    message::ws_request_message request(
        "placesws", op_to_str(Op::Batch_orders_op), args);
    requests[API::Place_batch_orders_ws]->ws_execute(std::move(request));
  }

  void place_orders_ws(std::vector<message::request_args>&& args) {
    message::ws_request_message request(
        "placesws", op_to_str(Op::Batch_orders_op), std::move(args));
    requests[API::Place_batch_orders_ws]->ws_execute(std::move(request));
  }

  void cancel_order_ws(const message::request_args& args) {
    message::ws_request_message request(
        "cancelws", op_to_str(Op::Cancel_order_op), args.args_format());
    requests[API::Cancel_order_ws]->ws_execute(std::move(request));
  }

  void cancel_order_ws(message::request_args&& args) {
    message::ws_request_message request(
        "cancelws", op_to_str(Op::Cancel_order_op), args.args_format());
    requests[API::Cancel_order_ws]->ws_execute(std::move(request));
  }

  void cancel_orders_ws(const std::vector<message::request_args>& args) {
    message::ws_request_message request(
        "cancelsws", op_to_str(Op::Batch_cancel_orders_op), args);
    requests[API::Cancel_batch_orders_ws]->ws_execute(std::move(request));
  }

  void cancel_orders_ws(std::vector<message::request_args>&& args) {
    message::ws_request_message request(
        "cancelsws", op_to_str(Op::Batch_cancel_orders_op), std::move(args));
    requests[API::Cancel_batch_orders_ws]->ws_execute(std::move(request));
  }

  void amend_order_ws(const message::request_args& args) {
    message::ws_request_message request(
        "amendws", op_to_str(Op::Amend_order_op), args.args_format());
    requests[API::Amend_order_ws]->ws_execute(std::move(request));
  }

  void amend_order_ws(message::request_args&& args) {
    message::ws_request_message request(
        "amendws", op_to_str(Op::Amend_order_op), args.args_format());
    requests[API::Amend_order_ws]->ws_execute(std::move(request));
  }

  void ammend_orders_ws(const message::request_args& args) {
    message::ws_request_message request(
        "amendsws", op_to_str(Op::Batch_amend_orders_op), args.args_format());
    requests[API::Amend_batch_orders_ws]->ws_execute(std::move(request));
  }

  void ammend_orders_ws(message::request_args&& args) {
    message::ws_request_message request(
        "amendsws", op_to_str(Op::Batch_amend_orders_op), args.args_format());
    requests[API::Amend_batch_orders_ws]->ws_execute(std::move(request));
  }

  void subscribe_order() {
    message::ws_request_message ws_request(
        op_to_str(Op::Subscribe_op),
        message::request_args::dyn_args_format({{"channel", "orders"},
                                                {"instType", "ANY"},
                                                {"instId", "BTC-USDT"}}));
    requests[API::Order_channel]->ws_execute(std::move(ws_request));
  }

  void unsubscribe_order() {
    message::ws_request_message ws_request(
        op_to_str(Op::Unsubscribe_op),
        message::request_args::dyn_args_format({{"channel", "orders"},
                                                {"instType", "ANY"},
                                                {"instId", "BTC-USDT"}}));

    requests[API::Order_channel]->ws_execute(std::move(ws_request));
  }

  void post_request(API api_request, const std::string& query,
                    const std::string& body,
                    gateway::ws_gateway_service::listener_t listener) {
    requests[api_request]->execute(
        {http::verb::post, query, login_headers(http::verb::post, query, body),
         body},
        listener);
  }

  void ws_request(API api_request, Op op, const std::string& args) {
    message::ws_request_message request("ws", op_to_str(op), args);
    requests[api_request]->ws_execute(std::move(request));
  }

  bool is_routing_api(API api_request) {
    return requests.contains(api_request);
  }

 private:
  network::headers_t login_headers(http::verb method, std::string_view query,
                                   std::string_view body) {
    std::string timestamp_utc = utils::get_utc_timestamp();
    std::string sign =
        utils::sing(SECRET_KEY, timestamp_utc, method, query, body);
    return {
        {"OK-ACCESS-KEY", API_KEY},
        {"OK-ACCESS-SIGN", sign},
        {"OK-ACCESS-TIMESTAMP", timestamp_utc},
        {"OK-ACCESS-PASSPHRASE", PASSPHRASE},
        {"x-simulated-trading", "1"},
    };
  }

  void inner_place_order(std::string&& body) {
    static std::string query = "/api/v5/trade/order";
    requests[API::Place_order]->execute(
        {http::verb::post, query, login_headers(http::verb::post, query, body),
         body},
        std::bind(&order_routing_handler_base::on_place_order, &*api_handler_,
                  std::placeholders::_1));
  }

  void inner_place_orders(std::string&& body) {
    static std::string query = "/api/v5/trade/batch-orders";
    requests[API::Place_batch_orders]->execute(
        {http::verb::post, query, login_headers(http::verb::post, query, body),
         body},
        std::bind(&order_routing_handler_base::on_place_batch_orders,
                  &*api_handler_, std::placeholders::_1));
  }

  void inner_cancel_order(std::string&& body) {
    static std::string query = "/api/v5/trade/cancel-order";
    requests[API::Cancel_order]->execute(
        {http::verb::post, query, login_headers(http::verb::post, query, body),
         body},
        std::bind(&order_routing_handler_base::on_cancel_order, &*api_handler_,
                  std::placeholders::_1));
  }

  void inner_cancel_orders(std::string&& body) {
    static std::string query = "/api/v5/trade/cancel-batch-orders";
    requests[API::Cancel_batch_orders]->execute(
        {http::verb::post, query, login_headers(http::verb::post, query, body),
         body},
        std::bind(&order_routing_handler_base::on_cancel_batch_orders,
                  &*api_handler_, std::placeholders::_1));
  }

  void inner_amend_order(std::string&& body) {
    static std::string query = "/api/v5/trade/amend-order";
    requests[API::Amend_order]->execute(
        {http::verb::post, query, login_headers(http::verb::post, query, body),
         body},
        std::bind(&order_routing_handler_base::on_amend_order, &*api_handler_,
                  std::placeholders::_1));
  }

  void inner_amend_orders(std::string&& body) {
    static std::string query = "/api/v5/trade/amend-batch-orders";
    requests[API::Amend_batch_orders]->execute(
        {http::verb::post, query, login_headers(http::verb::post, query, body),
         body},
        std::bind(&order_routing_handler_base::on_amend_batch_orders,
                  &*api_handler_, std::placeholders::_1));
  }
};
}  // namespace order_routing