#pragma once

#include <string>

namespace order_routing {
class order_routing_handler_base {
 public:
  virtual ~order_routing_handler_base() {}
  virtual void on_place_order(const std::string&) = 0;
  virtual void on_place_batch_orders(const std::string&) = 0;
  virtual void on_cancel_order(const std::string&) = 0;
  virtual void on_cancel_batch_orders(const std::string&) = 0;
  virtual void on_amend_order(const std::string&) = 0;
  virtual void on_amend_batch_orders(const std::string&) = 0;

  virtual void on_order_channel(const std::string&) = 0;

  virtual void on_place_order_ws(const std::string&) = 0;
  virtual void on_place_batch_orders_ws(const std::string&) = 0;
  virtual void on_cancel_order_ws(const std::string&) = 0;
  virtual void on_cancel_batch_orders_ws(const std::string&) = 0;
  virtual void on_amend_order_ws(const std::string&) = 0;
  virtual void on_amend_batch_orders_ws(const std::string&) = 0;
};
}  // namespace order_routing