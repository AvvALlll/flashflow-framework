#pragma once

#include <iostream>

#include "api/market-data/market_data_handler_base.hpp"

namespace market_data {
class market_data_test_handler : public market_data_handler_base {
  void on_get_tickers(const std::string&) override{};
  void on_get_ticker(const std::string&) override {
    // std::cout << response << std::endl;
  };
  void on_get_orderbook(const std::string&) override{};
  void on_get_full_orderbook(const std::string&) override{};
  void on_get_candlesticks(const std::string&) override{};
  void on_get_candlestiks_history(const std::string&) override{};
  void on_get_trades(const std::string&) override{};
  void on_get_trades_history(const std::string&) override{};
  void on_get_option_trades_instid(const std::string&) override{};
  void on_get_option_trades(const std::string&) override{};
  void on_get_total_volume(const std::string&) override{};
  void on_tickers(const std::string&) override {
    // std::cout << response << std::endl;
  };
  void on_candlesticks(const std::string&) override{};
  void on_trades(const std::string&) override{
    // std::cout << response << std::endl;
  };
};
}  // namespace market_data