#pragma once
#include <unordered_map>

#include "api_common/api_methods.hpp"
#include "api_common/constants.hpp"
#include "gateway/gateway_service_manager.hpp"
#include "gateway/request_api/base_request.hpp"
#include "gateway/request_api/http_request.hpp"
#include "gateway/request_api/ws_request.hpp"
#include "api/market-data/market_data_handler_base.hpp"
#include "network/http/http_service.hpp"

namespace market_data {

namespace http = boost::beast::http;

class market_data_api {
  metrics::metric_collector_creator collector_creator;
  gateway::gateway_service_manager gateway_manager;
  std::unique_ptr<market_data_handler_base> api_handler_;

  std::unordered_map<API, std::shared_ptr<request::base_request>> requests = {
      {API::Tickers, std::make_shared<request::http_request<API::Tickers>>(
                         gateway_manager, collector_creator)},
      {API::Ticker, std::make_shared<request::http_request<API::Ticker>>(
                        gateway_manager, collector_creator)},
      {API::Orderbook, std::make_shared<request::http_request<API::Orderbook>>(
                           gateway_manager, collector_creator)},
      {API::Full_orderbook,
       std::make_shared<request::http_request<API::Full_orderbook>>(
           gateway_manager, collector_creator)},
      {API::Candlesticks,
       std::make_shared<request::http_request<API::Candlesticks>>(
           gateway_manager, collector_creator)},
      {API::Candlesticks_history,
       std::make_shared<request::http_request<API::Candlesticks_history>>(
           gateway_manager, collector_creator)},
      {API::Trades, std::make_shared<request::http_request<API::Trades>>(
                        gateway_manager, collector_creator)},
      {API::Trades_history,
       std::make_shared<request::http_request<API::Trades_channel>>(
           gateway_manager, collector_creator)},
      {API::Option_trades_instid,
       std::make_shared<request::http_request<API::Option_trades_instid>>(
           gateway_manager, collector_creator)},
      {API::Option_trades,
       std::make_shared<request::http_request<API::Option_trades>>(
           gateway_manager, collector_creator)},
      {API::Total_volume,
       std::make_shared<request::http_request<API::Total_volume>>(
           gateway_manager, collector_creator)},
      {API::Tickers_channel,
       std::make_shared<request::ws_request<API::Tickers_channel>>(
           gateway_manager,
           std::bind(&market_data_handler_base::on_tickers, api_handler_.get(),
                     std::placeholders::_1),
           collector_creator)},
      {API::Candlesticks_channel,
       std::make_shared<request::ws_request<API::Candlesticks_channel>>(
           gateway_manager,
           std::bind(&market_data_handler_base::on_candlesticks,
                     api_handler_.get(), std::placeholders::_1),
           collector_creator)},
      {API::Trades_channel,
       std::make_shared<request::ws_request<API::Trades_channel>>(
           gateway_manager,
           std::bind(&market_data_handler_base::on_trades, api_handler_.get(),
                     std::placeholders::_1),
           collector_creator)}};

 public:
  explicit market_data_api(
      std::unique_ptr<market_data_handler_base> api_handler)
      : collector_creator(metric_folder_path, stage_filename),
        api_handler_(std::move(api_handler)) {}

  void reset_handler(std::unique_ptr<market_data_handler_base> new_handler) {
    api_handler_ = std::move(new_handler);
  }

  void stop() {
    for (auto& [request_id, request] : requests) {
      request->stop();
    }
  }

  void get_tickers(std::string query) {
    requests[API::Tickers]->execute(
        {http::verb::get, query},
        std::bind(&market_data_handler_base::on_get_tickers, &*api_handler_,
                  std::placeholders::_1));
  }

  void get_ticker(std::string_view query) {
    requests[API::Ticker]->execute(
        {http::verb::get, query},
        std::bind(&market_data_handler_base::on_get_ticker, &*api_handler_,
                  std::placeholders::_1));
  }

  void get_orderbook(std::string_view query) {
    requests[API::Orderbook]->execute(
        {http::verb::get, query},
        std::bind(&market_data_handler_base::on_get_orderbook, &*api_handler_,
                  std::placeholders::_1));
  }

  void get_full_orderbook(std::string_view query) {
    requests[API::Full_orderbook]->execute(
        {http::verb::get, query},
        std::bind(&market_data_handler_base::on_get_full_orderbook,
                  &*api_handler_, std::placeholders::_1));
  }

  void get_candlesticks(std::string_view query) {
    requests[API::Candlesticks]->execute(
        {http::verb::get, query},
        std::bind(&market_data_handler_base::on_get_candlesticks,
                  &*api_handler_, std::placeholders::_1));
  }

  void get_candlestiks_history(std::string_view query) {
    requests[API::Candlesticks_history]->execute(
        {http::verb::get, query},
        std::bind(&market_data_handler_base::on_get_candlestiks_history,
                  &*api_handler_, std::placeholders::_1));
  }

  void get_trades(std::string_view query) {
    requests[API::Trades]->execute(
        {http::verb::get, query},
        std::bind(&market_data_handler_base::on_get_trades, &*api_handler_,
                  std::placeholders::_1));
  }
  void get_trades_history(std::string_view query) {
    requests[API::Trades_history]->execute(
        {http::verb::get, query},
        std::bind(&market_data_handler_base::on_get_trades_history,
                  &*api_handler_, std::placeholders::_1));
  }

  void get_option_trades_instid(std::string_view query) {
    requests[API::Option_trades_instid]->execute(
        {http::verb::get, query},
        std::bind(&market_data_handler_base::on_get_option_trades_instid,
                  &*api_handler_, std::placeholders::_1));
  }

  void get_option_trades(std::string_view query) {
    requests[API::Option_trades]->execute(
        {http::verb::get, query},
        std::bind(&market_data_handler_base::on_get_option_trades,
                  &*api_handler_, std::placeholders::_1));
  }

  void get_total_volume(std::string_view query) {
    requests[API::Total_volume]->execute(
        {http::verb::get, query},
        std::bind(&market_data_handler_base::on_get_total_volume,
                  &*api_handler_, std::placeholders::_1));
  }

  void subscribe_tickers() {
    message::ws_request_message ws_request(
        op_to_str(Op::Subscribe_op),
        message::request_args::dyn_args_format(
            {{"channel", "tickers"}, {"instId", "BTC-USDT"}}));
    requests[API::Tickers_channel]->ws_execute(std::move(ws_request));
  }

  void unsubscribe_tickers() {
    message::ws_request_message ws_request(
        op_to_str(Op::Unsubscribe_op),
        message::request_args::dyn_args_format(
            {{"channel", "tickers"}, {"instId", "BTC-USDT"}}));

    requests[API::Tickers_channel]->ws_execute(std::move(ws_request));
  }

  void subscribe_candleticks() {
    message::ws_request_message ws_request(
        op_to_str(Op::Subscribe_op),
        message::request_args::dyn_args_format(
            {{"channel", "candle1D"}, {"instId", "BTC-USDT"}}));
    requests[API::Candlesticks_channel]->ws_execute(std::move(ws_request));
  }

  void unsubscribe_candleticks() {
    message::ws_request_message ws_request(
        op_to_str(Op::Unsubscribe_op),
        message::request_args::dyn_args_format(
            {{"channel", "candle1W"}, {"instId", "BTC-USDT"}}));

    requests[API::Candlesticks_channel]->ws_execute(std::move(ws_request));
  }

  void subscribe_trades() {
    message::ws_request_message ws_request(
        op_to_str(Op::Subscribe_op),
        message::request_args::dyn_args_format(
            {{"channel", "trades"}, {"instId", "BTC-USDT"}}));
    requests[API::Trades_channel]->ws_execute(std::move(ws_request));
  }

  void unsubscribe_trades() {
    message::ws_request_message ws_request(
        op_to_str(Op::Unsubscribe_op),
        message::request_args::dyn_args_format(
            {{"channel", "trades"}, {"instId", "BTC-USDT"}}));

    requests[API::Trades_channel]->ws_execute(std::move(ws_request));
  }

  bool is_market_api(API api_request) const noexcept {
    return requests.contains(api_request);
  }

  void get_request(API api_request, const std::string& query,
                   gateway::ws_gateway_service::listener_t listener) {
    requests[api_request]->execute({http::verb::get, query}, listener);
  }

  void post_request(API api_request, const std::string& query,
                    const std::string& body,
                    gateway::ws_gateway_service::listener_t listener) {
    requests[api_request]->execute(
        {http::verb::post, query, login_headers(http::verb::post, query, body),
         body},
        listener);
  }

  void ws_request(API api_request, Op op, const std::string& args) {
    requests[api_request]->ws_execute({op_to_str(op), args});
  }

   private:
  network::headers_t login_headers(http::verb method, std::string_view query,
                                   std::string_view body) {
    std::string timestamp_utc = utils::get_utc_timestamp();
    std::string sign =
        utils::sing(SECRET_KEY, timestamp_utc, method, query, body);
    return {
        {"OK-ACCESS-KEY", API_KEY},
        {"OK-ACCESS-SIGN", sign},
        {"OK-ACCESS-TIMESTAMP", timestamp_utc},
        {"OK-ACCESS-PASSPHRASE", PASSPHRASE},
        {"x-simulated-trading", "1"},
    };
  }
};
}  // namespace market_data