#pragma once

#include <string>

namespace market_data {
class market_data_handler_base {
 public:
  virtual ~market_data_handler_base() {}
  virtual void on_get_tickers(const std::string&) = 0;
  virtual void on_get_ticker(const std::string&) = 0;
  virtual void on_get_orderbook(const std::string&) = 0;
  virtual void on_get_full_orderbook(const std::string&) = 0;
  virtual void on_get_candlesticks(const std::string&) = 0;
  virtual void on_get_candlestiks_history(const std::string&) = 0;
  virtual void on_get_trades(const std::string&) = 0;
  virtual void on_get_trades_history(const std::string&) = 0;
  virtual void on_get_option_trades_instid(const std::string&) = 0;
  virtual void on_get_option_trades(const std::string&) = 0;
  virtual void on_get_total_volume(const std::string&) = 0;
  virtual void on_tickers(const std::string&) = 0;
  virtual void on_candlesticks(const std::string&) = 0;
  virtual void on_trades(const std::string&) = 0;
};
}  // namespace market_data