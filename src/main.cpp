#include <expected>
#include <utils/logging.hpp>

#include "metrics/collecting_system/base_collecting_system.hpp"
#include "metrics/collecting_system/rounting_collecting_system.hpp"
#include "metrics/collecting_system/socket_collecting_system.hpp"

// #define JSON_PARSE_EXAMPLE 1
// #define MARKET_API_EXAMPLE 1
// #define ORDER_ROUTING_API_EXAMPLE 1
#define COLLECTING_SYSTEM 1

int main() {
LOG_INIT;
#ifdef COLLECTING_SYSTEM
  std::shared_ptr<collecting_system::base_collecting_system> collecting_system =
      std::make_shared<collecting_system::routing_collecting_system>();

  collecting_system->run();
#endif
#ifdef ORDER_ROUTING_API_EXAMPLE
  message::request_args http_args({{"instId", "BTC-USDT"},
                                   {"tdMode", "cash"},
                                   {"side", "buy"},
                                   {"ordType", "limit"},
                                   {"px", "2.15"},
                                   {"sz", "2"},
                                   {"clOrdId", "placehttp"}});
  message::request_args ws_args({{"instId", "BTC-USDT"},
                                 {"tdMode", "cash"},
                                 {"side", "buy"},
                                 {"ordType", "limit"},
                                 {"px", "2.15"},
                                 {"sz", "2"},
                                 {"clOrdId", "placews"}});

  message::request_args http_amend_args({{"instId", "BTC-USDT"}, {"newSz", "3"}, {"clOrdId", "placehttp"}});
  message::request_args ws_amend_args({{"instId", "BTC-USDT"}, {"newSz", "3"}, {"clOrdId", "placews"}});

  order_routing::order_routing_api or_api(
      std::make_unique<order_routing::order_routing_handler_test>());
  sleep(1);
  or_api.subscribe_order();
  or_api.place_order_ws(ws_args);
  or_api.place_order(http_args);
  sleep(1);
  or_api.amend_order(http_amend_args);
  or_api.amend_order_ws(ws_amend_args);
  sleep(1);
  or_api.cancel_order({{{"instId", "BTC-USDT"}, {"clOrdId", "placehttp"}}});
  or_api.cancel_order_ws({{{"instId", "BTC-USDT"}, {"clOrdId", "placews"}}});
  sleep(1);
  or_api.stop();
  boost::asio::query(boost::asio::system_executor(),
                     boost::asio::execution::context)
      .join();
#endif

#ifdef MARKET_API_EXAMPLE
  market_data::market_data_api md_api(
      std::make_unique<market_data::market_data_test_handler>());
  md_api.subscribe_trades();
  md_api.get_ticker("/api/v5/market/ticker?instId=BTC-USD-SWAP");
  sleep(5);
  md_api.stop();
  boost::asio::query(boost::asio::system_executor(),
                     boost::asio::execution::context)
      .join();
#endif

#ifdef JSON_PARSE_EXAMPLE
  message::padded_string json_str = R"(
  {
    "arg": {
      "channel": "tickers",
    },
    "data": [
      {
        "instType": "SPOT",
        "test" : "123"
      }
    ]
  })"_padded;
  message::json_handler handler;
  auto res = handler.parse(json_str);
  auto res_res = res.and_then([](auto&& obj) { return obj.array("data"); });
  // .and_then([](auto&& obj) { return obj.object(0); });
  // .and_then([](auto&& obj) {return obj.next_string("test"); });
  for (message::ondemand::object&& obj : res_res.value()) {
    for (auto&& value : obj) std::cout << value << std::endl;
  }
  // if (res_res) {
  //   std::cout << res_res.value() << std::endl;
  // } else {
  //   std::cout << res_res.error() << std::endl;
  // }
#endif
  return 0;
}