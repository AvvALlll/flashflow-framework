#pragma once

#include <atomic>
#include <vector>

namespace utils {

template <size_t QUEUE_SIZE>
class circular_set {
  std::atomic_size_t cur_pos_ = 0;
  std::atomic_bool locks_[QUEUE_SIZE];
  std::atomic_size_t set_[QUEUE_SIZE];

 public:
  circular_set() {
    memset(locks_, 0, sizeof(locks_));
  }
  bool contains(std::size_t hashed_data, std::size_t cur_pos) {
    for (std::size_t i = cur_pos, j = QUEUE_SIZE >> 1; j != 0; --j) {
      while (locks_[i]);
      if (set_[i] == hashed_data) return true;
      if (i == 0) i = QUEUE_SIZE;
      if (--i == cur_pos) break;
    }
    return false;
  }

  bool push(const std::string& data) {
    std::size_t hashed_data = std::hash<std::string>{}(data);
    size_t tmp_cur_pos = (cur_pos_++) % QUEUE_SIZE;
    locks_[tmp_cur_pos] = true;
    set_[tmp_cur_pos] = hashed_data;
    locks_[tmp_cur_pos] = false;

    if (contains(hashed_data, tmp_cur_pos == 0 ? QUEUE_SIZE - 1 : tmp_cur_pos - 1)) return false;
    return true;
  }
};
}  // namespace utils