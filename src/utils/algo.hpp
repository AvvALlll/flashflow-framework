#pragma once
#include <openssl/hmac.h>
#include <openssl/sha.h>

#include <boost/beast/http.hpp>
#include <sstream>
#include <string>

namespace utils {
namespace http = boost::beast::http;  // from <boost/beast/http.hpp>

const char* base64_chars =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

#define EXTRA '='
#define MASK1 0xfc
#define MASK2 0x03
#define MASK3 0xf0
#define MASK4 0x0f
#define MASK5 0xc0
#define MASK6 0x3f

std::string base64_encode(unsigned char const* buf, unsigned int len) {
  std::stringstream encoded;
  unsigned int i = 0, j = 0, k = 0;
  unsigned char temp_a_3[3], temp_4[4];
  for (i = 0; i < len; i += 3) {
    for (j = i, k = 0; j < len && j < i + 3; j++) temp_a_3[k++] = *(buf++);
    for (; k < 3; k++) temp_a_3[k] = '\0';
    temp_4[0] = (temp_a_3[0] & MASK1) >> 2;
    temp_4[1] = ((temp_a_3[0] & MASK2) << 4) + ((temp_a_3[1] & MASK3) >> 4);
    temp_4[2] = ((temp_a_3[1] & MASK4) << 2) + ((temp_a_3[2] & MASK5) >> 6);
    temp_4[3] = temp_a_3[2] & MASK6;
    for (j = i, k = 0; j < len + 1 && j < i + 4; j++, k++)
      encoded << base64_chars[temp_4[k]];
    for (; k < 4; k++) encoded << EXTRA;
  }
  return encoded.str();
}

std::string sing(const std::string& secret_key, std::string_view timestamp,
                 http::verb method = http::verb::get,
                 std::string_view requestPath = "/users/self/verify", std::string_view body = "") {
  std::stringstream ss;

  ss << timestamp << method << requestPath << body;
  auto message = ss.str();

  unsigned int len;
  unsigned char* digest =
      HMAC(EVP_sha256(), secret_key.c_str(), secret_key.length(),
           (unsigned char*)message.c_str(), message.length(), nullptr, &len);
  std::string signature = base64_encode(digest, len);
  return signature;
}
}  // namespace utils
