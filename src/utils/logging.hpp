#pragma once

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <sstream>

namespace logging = boost::log;

#ifdef LOG_LEVEL
#define LOG_INIT                                                               \
  do {                                                                         \
    logging::add_common_attributes();                                          \
    logging::add_console_log(                                                  \
        std::clog, logging::keywords::format =                                 \
                       (logging::expressions::stream                           \
                        << "["                                                 \
                        << logging::expressions::format_date_time<             \
                               boost::posix_time::ptime>("TimeStamp",          \
                                                         "%Y-%m-%d %H:%M:%S")  \
                        << "][" << logging::trivial::severity << "]: "          \
                        << logging::expressions::smessage));           \
    logging::core::get()->set_filter(logging::trivial::severity >= LOG_LEVEL); \
  } while (0)

template <typename T>
concept outputtable = requires(T&& obj) {
  { std::declval<std::ostringstream&>() << std::forward<T>(obj) }
  ->std::same_as<std::ostream&>;
};

template <typename... T>
concept var_outputtable = (outputtable<T> || ...);

template <typename... T>
requires(sizeof...(T) >= 1 && var_outputtable<T...>) std::string_view
    print_log(std::ostringstream& oss, T&&... args) {
  ((oss << " " << std::forward<T>(args)), ...);
  return oss.view();
}

#define LOG_INFO(...)                                       \
  do {                                                      \
    std::ostringstream oss;                                 \
    BOOST_LOG_TRIVIAL(info) << print_log(oss, __VA_ARGS__); \
  } while (0)

#define LOG_DEBUG(...)                                       \
  do {                                                       \
    std::ostringstream oss;                                  \
    BOOST_LOG_TRIVIAL(debug) << print_log(oss, __VA_ARGS__); \
  } while (0)

#define LOG_TRACE(...)                                       \
  do {                                                       \
    std::ostringstream oss;                                  \
    BOOST_LOG_TRIVIAL(trace) << print_log(oss, __VA_ARGS__); \
  } while (0)

#define LOG_ERROR(...)                                       \
  do {                                                       \
    std::ostringstream oss;                                  \
    BOOST_LOG_TRIVIAL(error) << print_log(oss, __VA_ARGS__); \
  } while (0)

#else
#define LOG_INIT ;
#define LOG_DEBUG ;
#define LOG_INFO ;
#define LOG_TRACE ;
#define LOG_ERROR ;
#endif