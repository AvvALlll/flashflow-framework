#pragma once

#include <concepts>
#include <expected>
#include <string>
#include <string_view>
#include <type_traits>
#include <utility>

namespace utils {
template <typename T>
class ok {
  T ok_value_;

 public:
  ok(const T& ok_value) : ok_value_(ok_value) {}
  ok(T&& ok_value) : ok_value_(std::move(ok_value)) {}
  // template<typename U = T>
  // ok(U&& ok_value) noexcept(std::is_move_constructible_v<T>):
  // ok_value(std::forward<U>(ok_value)) {}

  const T& value() const { return ok_value_; }

  T& value() { return ok_value_; }

  T& operator*() { return ok_value_; }

  T* operator->() { return &ok_value_; }
};

template <>
class ok<void> {};

template <typename... Args>
concept is_appended = requires(Args&&... args) {
  (std::string().append(std::forward<Args>(args)), ...);
};

class error {
  std::string error_message;

 public:
  error(const error& err) : error_message(err.error_message) {}
  error(error&& err) : error_message(std::move(err.error_message)) {}
  template <typename... T>
  requires(is_appended<T...>) error(T&&... msgs) {
    (this->error_message.append(msgs), ...);
  }

  std::string_view text() const { return error_message; }

  friend std::ostream& operator<<(std::ostream& out, const error& err) {
    out << err.error_message;
    return out;
  }
};

template <typename T>
using ok_result = std::expected<ok<T>, error>;

template <typename T>
using result = std::expected<T, error>;

}  // namespace utils