#pragma once

#include <format>
#include <string>
#include <string_view>
#include <random>

namespace utils {

struct random_generator {
  std::random_device dev;
  std::mt19937 rng;
  std::uniform_int_distribution<std::mt19937::result_type> dist;
  random_generator(size_t from, size_t to): rng(dev()), dist(from, to) {}

  size_t generate() {
    return dist(rng);
  }
};

template<typename T> requires(requires(T&& t) {t.elapsed_time;})
uint64_t average(const std::vector<T>& metric_collection) noexcept {
  if (metric_collection.empty()) return 0;

  uint64_t average = 0;
  for (auto stat_info : metric_collection) average += stat_info.elapsed_time;
  return average / metric_collection.size();
}

template <typename... Args>
std::string dyn_format(std::string_view rt_fmt_str, Args&&... args) {
  return std::vformat(rt_fmt_str,
                      std::make_format_args(std::forward<Args>(args)...));
}

inline auto timestamp() {
  return duration_cast<std::chrono::seconds>(
             std::chrono::system_clock::now().time_since_epoch())
      .count();
}

inline auto timestamp_nano() {
  return duration_cast<std::chrono::nanoseconds>(
             std::chrono::system_clock::now().time_since_epoch())
      .count();
}

inline std::string get_utc_timestamp() {
  auto now = std::chrono::system_clock::now();
  auto now_time_t = std::chrono::system_clock::to_time_t(now);
  auto now_tm = std::gmtime(&now_time_t);
  char buf[32];
  std::strftime(buf, sizeof(buf), "%FT%T.123Z", now_tm);
  return std::string(buf);
}


}  // namespace utils