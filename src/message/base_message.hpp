#pragma once

#include <boost/algorithm/string/join.hpp>
#include <boost/json.hpp>
#include <chrono>
#include <initializer_list>
#include <iterator>
#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>

#include "utils/utils_function.hpp"

namespace message {

namespace json = boost::json;

class request_args {
  json::value args_;

 public:
  request_args(const json::value& args) : args_(args) {}
  request_args(const std::vector<request_args>& args_array) {
    args_.emplace_array();
    for (auto& args: args_array) {
      args_.as_array().push_back(args.args_);
    }
  }
  
  request_args(json::value&& args) : args_(std::move(args)) {}

  std::string args_format() const { return json::serialize(args_); }

  static std::string dyn_args_format(const json::value& args) {
    return json::serialize(args);
  }

  static std::string dyn_args_format(json::value&& args) {
    return json::serialize(std::move(args));
  }

  void clear() {}
};

class ws_request_message {
  const static inline std::string request_message_format =
      R"({{"op":"{}","args": [{}]}})";
  const static inline std::string request_message_format_with_id =
      R"({{"id":"{}","op":"{}","args": [{}]}})";

  std::optional<std::string_view> message_id_;
  std::string_view op_;
  std::vector<std::string> args_;

 public:
  ws_request_message(std::string_view op, std::vector<std::string>&& args)
      : op_(op), args_(std::move(args)) {}

  ws_request_message(std::string_view op, const std::vector<std::string>& args)
      : op_(op), args_(args) {}
  
  ws_request_message(std::string_view op, std::vector<request_args>&& args): op_(op) {
    for (auto& arg : args) {
      args_.push_back(arg.args_format());
    }
  }

   ws_request_message(std::string_view op, const std::vector<request_args>&& args): op_(op) {
    for (auto& arg : args) {
      args_.push_back(arg.args_format());
    }
  }

  ws_request_message(std::string_view op, std::string&& arg) : op_(op) {
    args_.push_back(std::move(arg));
  }
  ws_request_message(std::string_view op, const std::string& arg) : op_(op) {
    args_.push_back(arg);
  }

  ws_request_message(std::string_view message_id, std::string_view op,
                     std::vector<std::string>&& args)
      : message_id_(message_id), op_(op), args_(std::move(args)) {}

  ws_request_message(std::string_view message_id, std::string_view op,
                     const std::vector<std::string>& args)
      : message_id_(message_id), op_(op), args_(args) {}

  ws_request_message(std::string_view message_id, std::string_view op,
                     std::vector<request_args>&& args)
      : message_id_(message_id), op_(op) {
    for (auto& arg : args) {
      args_.push_back(arg.args_format());
    }
  }

  ws_request_message(std::string_view message_id, std::string_view op,
                     const std::vector<request_args>& args)
      : message_id_(message_id), op_(op) {
    for (auto& arg : args) {
      args_.push_back(arg.args_format());
    }
  }

  ws_request_message(std::string_view message_id, std::string_view op,
                     std::string&& arg)
      : message_id_(message_id), op_(op) {
    args_.push_back(std::move(arg));
  }
  ws_request_message(std::string_view message_id, std::string_view op,
                     const std::string& arg)
      : message_id_(message_id), op_(op) {
    args_.push_back(arg);
  }

  std::string_view id() const noexcept { return message_id_.value(); }

  std::string_view op() const noexcept { return op_; }

  const std::vector<std::string>& args() const noexcept { return args_; }

  std::string create_message() const {
    if (message_id_)
      return utils::dyn_format(request_message_format_with_id,
                               message_id_.value(), op_,
                               boost::join(args_, ","));

    return utils::dyn_format(request_message_format, op_,
                             boost::join(args_, ","));
  }

  bool is_subscribe() const noexcept { return op_ == "subscribe"; }

  bool is_unsubscribe() const noexcept { return op_ == "unsubscribe"; }

  friend auto operator<=>(const ws_request_message& lhs,
                          const ws_request_message& rhs) {
    return lhs.args() <=> rhs.args();
  }
};
}  // namespace message