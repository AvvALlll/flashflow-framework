#pragma once
#include <expected>

#include "simdjson.h"
#include "simdjson/padded_string.h"
#include "simdjson/padded_string_view.h"
#include "utils/error.hpp"

namespace message {
using namespace simdjson;

template <typename T>
struct json_element {
  T element;

  utils::result<json_element<ondemand::object>> object(std::string_view key);
  utils::result<json_element<ondemand::array>> array(std::string_view key);
  utils::result<std::string_view> next_string(std::string_view key);

  auto begin() {
    return element.begin().value();
  }

  auto end() {
    return element.end().value();
  }
};

template <>
struct json_element<typename ondemand::array> {
  ondemand::array element;

  utils::result<json_element<ondemand::object>> object(size_t index) {
    json_element<ondemand::object> next_obj;
    auto err = element.at(index).get(next_obj.element);
    if (err) {
      return std::unexpected(utils::error(error_message(err)));
    }
    return std::move(next_obj);
  }

  utils::result<json_element<ondemand::array>> array(size_t index) {
    json_element<ondemand::array> next_arr;
    auto err = element.at(index).get_array().get(next_arr.element);
    if (err) {
      return std::unexpected(utils::error(error_message(err)));
    }
    return std::move(next_arr);
  }

  utils::result<std::string_view> next_string(size_t index) {
    ondemand::value val;
    auto err = element.at(index).get(val);
    if (err) {
      return std::unexpected(utils::error(error_message(err)));
    }
    return val.get_string().take_value();
  }


  auto begin() {
    return element.begin().value();
  }

  auto end() {
    return element.end().value();
  }

};

template <typename T>
utils::result<json_element<ondemand::object>> json_element<T>::object(
    std::string_view key) {
  json_element<ondemand::object> next_obj;
  auto err = element[key].get(next_obj.element);
  if (err) {
    return std::unexpected(utils::error(error_message(err)));
  }
  return std::move(next_obj);
}

template <typename T>
utils::result<json_element<ondemand::array>> json_element<T>::array(
    std::string_view key) {
  json_element<ondemand::array> next_arr;
  auto err = element[key].get_array().get(next_arr.element);
  if (err) {
    return std::unexpected(utils::error(error_message(err)));
  }
  return std::move(next_arr);
}

template <typename T>
utils::result<std::string_view> json_element<T>::next_string(
    std::string_view key) {
  ondemand::value val;
  auto err = element[key].get(val);
  if (err) {
    return std::unexpected(utils::error(error_message(err)));
  }
  return val.get_string().take_value();
}

class json_handler {
  ondemand::parser parser_;
  padded_string cur_parse_str;

 public:
  utils::result<json_element<ondemand::document>> parse(
    std::string_view padded_json_str) {
    json_element<ondemand::document> doc;
    cur_parse_str = padded_json_str;
    error_code err = parser_.iterate(cur_parse_str).get(doc.element);
    if (err) {
      return std::unexpected(utils::error{error_message(err)});
    }
    return doc;
  }

  static utils::result<json_element<ondemand::document>> dyn_parse(
      padded_string json_str) {
    ondemand::parser parser;
    json_element<ondemand::document> doc;

    error_code err = parser.iterate(json_str).get(doc.element);
    if (err) {
      return std::unexpected(utils::error{error_message(err)});
    }
    return doc;
  }
};
}  // namespace message